A C++ library for calculating the analytic displacement and strain
due to rectangular (Okada) or triangular (Nikkhoo M. and Walter T.R.)
fault patches.  C and Fortran bindings are also provided.

INSTALL
* To build, you will need

  C, C++, and Fortran compilers
  python
  FTensor Library: https://bitbucket.org/wlandry/ftensor

* The build system uses waf.  To see the available options, run

  ./waf -h

* To configure, run

  ./waf configure

* To build, run

  ./waf

* To install, run

  ./waf install

=================
Using the library
=================

* C++
** You will need to create okada::Okada or okada::Triangle_Fault
   classes.  You can see the declaration of these classes in
     src/okada/Okada.hxx
     src/okada/Triangular_Fault.hxx
** There are examples of usage in
     test/okada/okada_cxx.cxx
     test/triangle/triangle_cxx.cxx

* C
** There are only two pure C functions
     okada_displacement
     triangle_displacement
** They are declared in the file
     src/okada_c.h
** They forward the arguments to the C++ classes and return results.
** If there are any errors, the library exits the program.
** There are examples in
     test/okada/okada_c.c
     test/triangle/triangle_c.c
** To use the library in your own code, you will need to link to both
   the libokada_c and libokada libraries.


* Fortran
** The functions to call are 
     okada_displacement_f
     triangle_displacement_f
** Name mangling between Fortran and C is setup during the configure step.
** The order of arguments is the same as the C interface.
** If there are any errors, the library exits the program.
** There are examples in
     test/okada/okada_f90.f90
     test/triangle/triangle_f90.f90
** As with the C interface, to use the library in your own code, you
   will need to link to both the libokada_c and libokada libraries.
