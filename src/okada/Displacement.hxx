#pragma once

#include <FTensor.hpp>

#include <utility>

namespace okada
{
  typedef std::pair<FTensor::Tensor1<double,3>,
                    FTensor::Tensor2_symmetric<double,3> >
  Displacement;
}

