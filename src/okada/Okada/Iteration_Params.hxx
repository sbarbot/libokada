#pragma once

#include "../Okada.hxx"

namespace okada
{
  struct Iteration_Params
  {
    double xi, et, q, xi2, et2, q2, r2, r, r3, r5, y, d, tt, alx, ale,
      x11, y11, x32, y32, ey, ez, fy, fz, gy, gz, hy, hz;

    Iteration_Params(const double &Xi, const double &Et,
                     const double &Q, const bool &kxi,
                     const bool &ket,
                     const double &sin_dip,
                     const double &cos_dip);
    std::pair<FTensor::Tensor1<double,3>,
              FTensor::Tensor2<double,3,3> >
    ua(const FTensor::Tensor1<double,3> &dislocation,
       const double &alp1, const double &alp2,
       const double &sin_dip, const double &cos_dip) const;
    std::pair<FTensor::Tensor1<double,3>,
              FTensor::Tensor2<double,3,3> >
    ub(const FTensor::Tensor1<double,3> &dislocation,
       const double &alp3,
       const double &sin_dip,
       const double &cos_dip,
       const double &cos_dip2,
       const double &sin_dip2,
       const double &sin_cos_dip) const;
    std::pair<FTensor::Tensor1<double,3>,
              FTensor::Tensor2<double,3,3> >
    uc(const double &z,
       const FTensor::Tensor1<double,3> &dislocation,
       const double &alp4, const double &alp5,
       const double &sin_dip, const double &cos_dip,
       const double &cos_dip2,
       const double &sin_dip2,
       const double &sin_cos_dip) const;
  };
}
