#include "../Okada.hxx"

namespace okada
{
  Displacement Okada::displacement(const FTensor::Tensor1<double,3> &coord)
  {
    FTensor::Tensor1<double,3> transformed;
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    FTensor::Index<'k',3> k;
    FTensor::Index<'l',3> l;
  
    transformed(i)=rotation_strike(i,j)*(coord(j)-origin(j));
    /* z gets offset later depending on whether we are calculating the
       contribution from the source or the image. */
    transformed(2)=-coord(2);
    Displacement rotated_result(dc3d(transformed));
    Displacement result(rotated_result);

    result.first(i)=rotation_strike(i,j)*rotated_result.first(j);
    result.second(i,j)=
      (rotation_strike(i,k)*rotated_result.second(k,l))^rotation_strike(l,j);
    return result;
  }
}
