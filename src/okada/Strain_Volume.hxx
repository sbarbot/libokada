#pragma once

#include "Displacement.hxx"

namespace okada
{
  class Strain_Volume
  {
  public:
    FTensor::Tensor1<double,3> origin, shape;
    FTensor::Tensor2<double,3,3> rotation_strike;
    FTensor::Tensor2_symmetric<double,3> stress, rotated_applied_strain;
    double nu, scale;
  
    /// The convention is that the corner of the region is at
    /// x=origin(0)
    /// y=origin(1) - shape(1)/2
    /// z=origin(2))
    Strain_Volume(const FTensor::Tensor1<double,3> &Origin,
                  const FTensor::Tensor1<double,3> &Shape,
                  const double &Strike,
                  const FTensor::Tensor2_symmetric<double,3> &applied_strain,
                  const double &lambda, const double &mu);

    Displacement displacement(const FTensor::Tensor1<double,3> &coord) const;

    FTensor::Tensor1<double,3> IU(const FTensor::Tensor1<double,3> &xyz,
                                  const FTensor::Tensor1<double,3> &y) const;
    FTensor::Tensor2<double,3,3>
    dIU(const FTensor::Tensor1<double,3> &xyz,
        const FTensor::Tensor1<double,3> &y) const;

    bool is_inside_strain_volume (const FTensor::Tensor1<double,3> &rotated_xyz)
      const
    {
      /// The convention is that the corner of the region is at 0,-shape(1)/2,0
      return rotated_xyz(0)>=0 && rotated_xyz(0)<=shape(0)
        && rotated_xyz(1)>=-shape(1)/2 && rotated_xyz(1)<=shape(1)/2
        && rotated_xyz(2) - origin(2)>=0
        && rotated_xyz(2) - origin(2)<=shape(2);
    }
};
}
