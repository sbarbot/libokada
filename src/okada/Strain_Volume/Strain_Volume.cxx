#include "../Strain_Volume.hxx"
#include "../radians.hxx"

#include <stdexcept>

namespace okada
{
  Strain_Volume::Strain_Volume(const FTensor::Tensor1<double,3> &Origin,
                               const FTensor::Tensor1<double,3> &Shape,
                               const double &Strike,
                               const FTensor::Tensor2_symmetric<double,3>
                               &applied_strain,
                               const double &lambda, const double &mu)
    : origin(Origin), shape(Shape),
      rotation_strike(cos(radians(Strike)),sin(radians(Strike)),0,
                      -sin(radians(Strike)),cos(radians(Strike)),0,
                      0,0,1),
      nu(lambda/(2*(lambda+mu))),
      scale (1.0/(16*(1-nu)*M_PI*mu))
  {
    if(nu<-1 || nu>0.5)
      { throw std::runtime_error("In Strain_Volume, nu=lambda/(2*(lambda+mu)) "
                                 "must be between -1 and 0.5"); }
    if(origin(2)<0)
      { throw std::runtime_error("Strain_Volume depth must be greater than 0"); }

    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    FTensor::Index<'k',3> k;
    FTensor::Index<'l',3> l;
    FTensor::Tensor2_symmetric<double,3> delta(1,0,0,1,0,1);
    stress(i,j) = lambda*applied_strain(k,k)*delta(i,j)
      + 2*mu*applied_strain(i,j);
    rotated_applied_strain(i,j) = (applied_strain(k,l)*rotation_strike(k,i))
      ^rotation_strike(l,j);
  }
}
