#include "Integrals.hxx"
#include "../../Strain_Volume.hxx"

namespace okada
{
  FTensor::Tensor1<double,3>
  Strain_Volume::IU(const FTensor::Tensor1<double,3> &xyz,
                    const FTensor::Tensor1<double,3> &y) const
  {
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    FTensor::Index<'k',3> k;
    FTensor::Tensor1<double,3> result;
    result(i)=stress(j,k)*Integrals(xyz,y,nu).J()(i,j,k);
    return result;
  }
}
