#pragma once

#include <FTensor.hpp>

#include <vector>
#include <cmath>
#include <math.h>
#include <complex.h>

namespace okada
{
  class Integrals
  {
  public:
    const FTensor::Tensor1<double,3> &xyz, &y;
    double nu, r1, r2;
    
    Integrals(const FTensor::Tensor1<double,3> &Xyz,
              const FTensor::Tensor1<double,3> &Y,
              const double &Nu)
      :xyz(Xyz), y(Y), nu(Nu)
    {
      FTensor::Index<'i',3> i;
      FTensor::Tensor1<double,3> diff;
      diff(i)=xyz(i)-y(i);
      r1=diff.l2();
      diff(2)=xyz(2)+y(2);
      r2=diff.l2();
    }

    FTensor::Tensor3<double,3,3,3> J() const
    {
      return FTensor::Tensor3<double,3,3,3>
        (Jxxx(0),Jxxy(0),Jxxz(0),
         Jxyx(0),Jxyy(0),Jxyz(),
         Jxzx(0),Jxzy(),Jxzz(0),

         Jxyy(1),Jxyx(1),Jxyz(),
         Jxxy(1),Jxxx(1),Jxxz(1),
         Jxzy(),Jxzx(1),Jxzz(1),

         Jzxx(0),Jzxy(),Jzxz(0),
         Jzxy(),Jzxx(1),Jzxz(1),
         Jzzx(0),Jzzx(1),Jzzz());
    }
    

    double Jxxx(const int &ix) const;
    double Jxxy(const int &ix) const;
    double Jxxz(const int &ix) const;
    double Jxyx(const int &ix) const;
    double Jxyy(const int &ix) const;
    double Jxyz() const;
    double Jxzx(const int &ix) const;
    double Jxzy() const;
    double Jxzz(const int &ix) const;
    double Jzxx(const int &ix) const;
    double Jzxy() const;
    double Jzxz(const int &ix) const;
    double Jzzx(const int &ix) const;
    double Jzzz() const;

    std::vector<FTensor::Tensor3<double,3,3,3> > dJ() const;
    
    FTensor::Tensor1<double,3> dJxxx(const int &ix) const;
    FTensor::Tensor1<double,3> dJxxy(const int &ix) const;
    FTensor::Tensor1<double,3> dJxxz(const int &ix) const;
    FTensor::Tensor1<double,3> dJxyx(const int &ix) const;
    FTensor::Tensor1<double,3> dJxyy(const int &ix) const;
    FTensor::Tensor1<double,3> dJxyz(const int &ix) const;
    FTensor::Tensor1<double,3> dJxzx(const int &ix) const;
    FTensor::Tensor1<double,3> dJxzy(const int &ix) const;
    FTensor::Tensor1<double,3> dJxzz(const int &ix) const;
    FTensor::Tensor1<double,3> dJzxx(const int &ix) const;
    FTensor::Tensor1<double,3> dJzxy(const int &ix) const;
    FTensor::Tensor1<double,3> dJzxz(const int &ix) const;
    FTensor::Tensor1<double,3> dJzzx(const int &ix) const;
    FTensor::Tensor1<double,3> dJzzz() const;
    
    double acoth(const double &x) const
    {
      /// The imaginary components all cancel out.

      /// This uses C99 functions because we can not guarantee that we
      /// have C++11 support
      return creal(catanh(1/x));
    }
    
    double atan3(const double &y, const double &x) const
    {
      return (x!=0 ? atan(y/x) : (y>0 ? M_PI/2 : -M_PI/2));
    }
    
    double xLogy(const double &x, const double &y) const
    {
      return (x==0 ? 0 : x*std::log(y));
    }
  };
}
