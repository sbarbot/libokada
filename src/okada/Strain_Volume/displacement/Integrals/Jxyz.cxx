#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jxyz() const
  {
    const double x3(xyz(2)), y3(y(2));
      
    return (-r1+(1+8*(nu-1)*nu)*r2-2*(1/r2)*x3*y3+xLogy((-4)*(nu-1)*((-1)+2*nu)*(x3+y3),r2+x3+y3));
  }
}
