#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jzxy() const
  {
    const double x3(xyz(2)), y3(y(2));
      
    double result (r1+((-1)-8*(nu-1)*nu)*r2-2*(1/r2)*x3*y3
                   + xLogy(2*(3*x3+2*y3-6*nu*(x3+y3)+4*nu*nu*(x3+y3)),
                           r2+x3+y3));
    if (x3!=0)
      { result+=2*((-3)+4*nu)*x3*acoth((1/r2)*(x3+y3)); }
    return -result;
  }
}
