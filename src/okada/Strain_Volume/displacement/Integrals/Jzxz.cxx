#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jzxz(const int &ix) const
  {
    const int iy(1-ix);
    const double x1(xyz(ix)), x2(xyz(iy)), x3(xyz(2)), y1(y(ix)), y2(y(iy)), y3(y(2));
      
    return -1*((-2)*(1/r2)*x3*(x2-y2)*y3*(x3+y3)
               * (1/((x1-y1)*(x1-y1)+(x3+y3)*(x3+y3)))
               + 4*(nu-1)*((-1)+2*nu)*(x1-y1)*atan3((x1-y1),(x2-y2))
               + 4*(nu-1)*((-1)+2*nu)*(x1-y1)*atan2(r2*(-x1+y1),(x2-y2)*(x3+y3))
               + xLogy((-4)*(nu-1)*((-1)+2*nu)*(x2-y2),r2+x3+y3)
               + xLogy(x3-y3,r1+x2-y2)
               + xLogy(-x3-7*y3-8*nu*nu*(x3+y3)+8*nu*(x3+2*y3),r2+x2-y2));
  }
}
