#include "../Integrals.hxx"

namespace okada
{
  double Integrals::Jzzx(const int &ix) const
  {
    const int iy(1-ix);
    const double x1(xyz(ix)), x2(xyz(iy)), x3(xyz(2)),
      y1(y(ix)), y2(y(iy)), y3(y(2));

    return (2/r2)*x3*(x2-y2)*y3*(x3+y3)/((x1-y1)*(x1-y1)+(x3+y3)*(x3+y3))
      + 5*x1*atan2((-5)*x1,x2-y2)
      - 3*x1*atan2(3*x1,x2-y2) 
      + 4*nu*x1*atan2(-nu*x1,x2-y2)
      - 12*nu*x1*atan2(3*nu*x1,x2-y2)
      + 8*nu*nu*x1*atan2(-nu*nu*x1,x2-y2)
      - 4*((-1)+nu)*(x1-y1)*atan2(r1*(x1-y1),(x2-y2)*(x3-y3))
      - 8*(-1+nu)*(-1+nu)*(x1-y1)*atan2(r2*(x1-y1),(x2-y2)*(x3+y3))
      + 3*y1*atan2((-3)*y1,x2-y2)
      - 5*y1*atan2(5*y1,x2-y2)
      + 12*nu*y1*atan2((-3)*nu*y1,x2-y2)
      - 4*nu*y1*atan2(nu*y1,x2-y2)
      - 8*nu*nu*y1*atan2(nu*nu*y1,x2-y2)
      + xLogy((-4)*x3,r2-x2+y2)
      + xLogy((-4)*(-1+nu)*(x2-y2),r1+x3+(-1)*y3)
      + xLogy((-8)*(-1+nu)*(-1+nu)*(x2-y2),r2+x3+y3)
      + xLogy((-1)*((-3)+4*nu)*(x3-y3),r1+x2-y2)
      + xLogy((-7)*x3-5*y3+12*nu*(x3+y3)-8*nu*nu*(x3+y3),r2+x2-y2);
  }
}
