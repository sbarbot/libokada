#include "../../Integrals.hxx"

namespace okada
{
  std::vector<FTensor::Tensor3<double,3,3,3> > Integrals::dJ() const
    {
      std::vector<FTensor::Tensor3<double,3,3,3> > result;

      FTensor::Tensor1<double,3> xxx(dJxxx(0)), xxy(dJxxy(0)), xxz(dJxxz(0)),
        xyx(dJxyx(0)), xyy(dJxyy(0)), xyz(dJxyz(0)),
        xzx(dJxzx(0)), xzy(dJxzy(0)), xzz(dJxzz(0)),
        yxx(dJxyy(1)), yxy(dJxyx(1)), yxz(dJxyz(1)),
        yyx(dJxxy(1)), yyy(dJxxx(1)), yyz(dJxxz(1)),
        yzx(dJxzy(1)), yzy(dJxzx(1)), yzz(dJxzz(1)),
        zxx(dJzxx(0)), zxy(dJzxy(0)), zxz(dJzxz(0)),
        zyx(dJzxy(1)), zyy(dJzxx(1)), zyz(dJzxz(1)),
        zzx(dJzzx(0)), zzy(dJzzx(1)), zzz(dJzzz());

      for(int i=0; i<3; ++i)
        {
          result.push_back
            (FTensor::Tensor3<double,3,3,3>(xxx(i), xxy(i), xxz(i),
                                            xyx(i), xyy(i), xyz(i),
                                            xzx(i), xzy(i), xzz(i),

                                            yxx(i), yxy(i), yxz(i),
                                            yyx(i), yyy(i), yyz(i),
                                            yzx(i), yzy(i), yzz(i),

                                            zxx(i), zxy(i), zxz(i),
                                            zyx(i), zyy(i), zyz(i),
                                            zzx(i), zzy(i), zzz(i)));
        }
      return result;
    }
}
