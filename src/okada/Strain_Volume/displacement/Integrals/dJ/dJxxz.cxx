#include "../../Integrals.hxx"

namespace okada
{
  FTensor::Tensor1<double, 3> Integrals::dJxxz (const int &ix) const
  {
    const int iy (1 - ix);
    const double x1 (xyz (ix)), x2 (xyz (iy)), x3 (xyz (2)), y1 (y (ix)),
      y2 (y (iy)), y3 (y (2)), d1 (x1 - y1), d2 (x2 - y2), d3 (x3 - y3),
      dp3 (x3 + y3), dr12_inv (1 / (d1 * d1 + d2 * d2)),
      dr13_inv (1 / (d1 * d1 + d3 * d3)), dr13p (d1 * d1 + dp3 * dp3),
      dr13p_inv (1 / dr13p), dr23_inv (1 / (d2 * d2 + d3 * d3)),
      dr23p (d2 * d2 + dp3 * dp3), dr23p_inv (1 / dr23p),
      r1xy1_inv (1 / (r1 + x1 - y1)), r1xy2_inv (1 / (r1 + x2 - y2)),
      r2xy1_inv (1 / (r2 + x1 - y1)),
      r2xy2_inv (1 / (r2 + x2 - y2)), 
      r1_inv (1 / r1), r2_inv (1 / r2),
      dr123p_32 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -1.5));

    FTensor::Tensor1<double, 3> result;
    /// d/dx
    result (ix)
      = - 4 * (nu - 1) * r1xy1_inv * d2
        - 4 * (nu - 1) * r1_inv * d1 * r1xy1_inv * d2
        - 4 * (nu - 1) * r2xy1_inv * d2
        - 4 * (nu - 1) * r2_inv * d1 * r2xy1_inv * d2
        - ((-3) + 4 * nu) * r1_inv * d1 * d1 * r1xy2_inv
        + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d1 * d1 * r2xy2_inv
        + 4 * (nu - 1) * r1 * d2 * dr13_inv * dr23_inv * d3 * d3
        - 4 * (nu - 1) * r1_inv * d1 * d1 * d2 * dr13_inv * dr23_inv * d3 * d3
        + 4 * (nu - 1) * ((-1) + 2 * nu) * dr12_inv * d2 * dp3
        - 4 * r2_inv * x3 * d1 * d1 * d2 * y3 * dr13p_inv * dr13p_inv
        + 2 * r2_inv * x3 * d2 * y3 * dr13p_inv
        - 4 * (nu - 1) * ((-1) + 2 * nu) * r2 * dr12_inv * d2 * dp3 * dp3
          * dr13p_inv
        + (-4) * (nu - 1) * ((-1) + 2 * nu) * r2_inv * d1 * d1 * dr12_inv * d2
          * dp3 * dp3 * dr13p_inv
        + 4 * (nu - 1) * r2 * d2 * dp3 * dp3 * dr13p_inv * dr23p_inv
        - 4 * (nu - 1) * r2_inv * d1 * d1 * d2 * dp3 * dp3 * dr13p_inv
          * dr23p_inv - 2 * x3 * d1 * d1 * d2 * y3 * dr13p_inv * dr123p_32
        + xLogy (3 - 4 * nu, r1 + x2 - y2)
        + xLogy (5 + 4 * nu * ((-3) + 2 * nu), r2 + x2 - y2);

    /// d/dy
    result (iy)
      = (-4) * (nu - 1) * r1_inv * r1xy1_inv * d2 * d2
        - 4 * (nu - 1) * r2_inv * r2xy1_inv * d2 * d2
        - ((-3) + 4 * nu) * d1 * r1xy2_inv
        - ((-3) + 4 * nu) * r1_inv * d1 * d2 * r1xy2_inv
        + (5 + 4 * nu * ((-3) + 2 * nu)) * d1 * r2xy2_inv
        + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d1 * d2 * r2xy2_inv
        + 4 * (nu - 1) * r1 * d1 * dr13_inv * dr23_inv * d3 * d3
        - 4 * (nu - 1) * r1_inv * d1 * d2 * d2 * dr13_inv * dr23_inv * d3 * d3
        - 4 * (nu - 1) * ((-1) + 2 * nu) * d1 * dr12_inv * dp3
        + 2 * r2_inv * x3 * d1 * y3 * dr13p_inv
        + 4 * (nu - 1) * ((-1) + 2 * nu) * r2 * d1 * dr12_inv * dp3 * dp3
          * dr13p_inv
        - 4 * (nu - 1) * ((-1) + 2 * nu) * r2_inv * d1 * dr12_inv * d2 * d2
          * dp3 * dp3 * dr13p_inv
        + 4 * (nu - 1) * r2 * d1 * dp3 * dp3 * dr13p_inv * dr23p_inv
        - 4 * (nu - 1) * r2_inv * d1 * d2 * d2 * dp3 * dp3 * dr13p_inv
          * dr23p_inv - 2 * x3 * d1 * d2 * d2 * y3 * dr13p_inv * dr123p_32
        + xLogy (4 - 4 * nu, r1 + x1 - y1) + xLogy (4 - 4 * nu, r2 + x1 - y1);
    /// d/dz
    result (2)
      = - 4 * (nu - 1) * r1_inv * r1xy1_inv * d2 * d3
        - ((-3) + 4 * nu) * r1_inv * d1 * r1xy2_inv * d3
        + (-4) * (nu - 1) * r1 * d1 * d2 * dr13_inv * dr23_inv * d3
        - 4 * (nu - 1) * r1_inv * d1 * d2 * dr13_inv * dr23_inv * d3 * d3 * d3
        - 4 * (nu - 1) * r2_inv * r2xy1_inv * d2 * dp3
        + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d1 * r2xy2_inv * dp3
        - 4 * r2_inv * x3 * d1 * d2 * y3 * dp3 * dr13p_inv * dr13p_inv
        + 2 * r2_inv * d1 * d2 * y3 * dr13p_inv
        + 4 * (nu - 1) * ((-1) + 2 * nu) * r2 * d1 * dr12_inv * d2 * dp3
          * dr13p_inv
        - 4 * (nu - 1) * ((-1) + 2 * nu) * r2_inv * d1 * dr12_inv * d2 * dp3
          * dp3 * dp3 * dr13p_inv
        - 4 * (nu - 1) * r2 * d1 * d2 * dp3 * dr13p_inv * dr23p_inv
        - 4 * (nu - 1) * r2_inv * d1 * d2 * dp3 * dp3 * dp3 * dr13p_inv
          * dr23p_inv - 2 * x3 * d1 * d2 * y3 * dp3 * dr13p_inv * dr123p_32
        + 4 * (nu - 1) * ((-1) + 2 * nu) * atan2 (r2 * (-x1 + y1), d2 * dp3)
        + (4 + (-4) * nu) * atan2 (r1 * d3, d1 * d2)
        + (4 - 4 * nu) * atan2 (r2 * dp3, d1 * d2);
    return result;
  }
}
