#include "../../Integrals.hxx"

namespace okada
{
  FTensor::Tensor1<double, 3> Integrals::dJzzz () const
  {
    const int ix (0);
    const int iy (1 - ix);
    const double x1 (xyz (ix)), x2 (xyz (iy)), x3 (xyz (2)), y1 (y (ix)),
      y2 (y (iy)), y3 (y (2)), d1 (x1 - y1), d2 (x2 - y2), d3 (x3 - y3),
      dp3 (x3 + y3),
      dr13_inv (1 / (d1 * d1 + d3 * d3)),
      dr13p (d1 * d1 + dp3 * dp3),
      dr13p_inv (1 / dr13p),
      dr23_inv (1 / (d2 * d2 + d3 * d3)),
      dr23p (d2 * d2 + dp3 * dp3), dr23p_inv (1 / dr23p),
      r1xy1_inv (1 / (r1 + x1 - y1)), 
      r1xy2_inv (1 / (r1 + x2 - y2)),
      r2xy1_inv (1 / (r2 + x1 - y1)),
      r2xy2_inv (1 / (r2 + x2 - y2)), 
      r1_inv (1 / r1), r2_inv (1 / r2),
      dr123p_32 (pow (d1 * d1 + d2 * d2 + dp3 * dp3, -1.5));
      
    FTensor::Tensor1<double, 3> result;
    /// d/dx
    result (ix)
      = - ((-3) + 4 * nu) * r1xy1_inv * d2
      - ((-3) + 4 * nu) * r1_inv * d1 * r1xy1_inv * d2
      + (5 + 4 * nu * ((-3) + 2 * nu)) * r2xy1_inv * d2
      + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d1 * r2xy1_inv * d2
      - ((-3) + 4 * nu) * r1_inv * d1 * d1 * r1xy2_inv
      + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d1 * d1 * r2xy2_inv
      + 2 * ((-1) + 2 * nu) * r1 * d2 * dr13_inv * dr23_inv * d3 * d3
      - 2 * ((-1) + 2 * nu) * r1_inv * d1 * d1 * d2 * dr13_inv * dr23_inv
      * d3 * d3
      - 2 * (1 - 2 * nu) * (1 - 2 * nu) * r2 * d2 * dp3 * dp3 * dr13p_inv
      * dr23p_inv
      - 4 * r2_inv * x3 * d1 * d1 * d2 * y3 * dr13p_inv * dr13p_inv
      * dr23p_inv * (d1 * d1 + d2 * d2 + 2 * dp3 * dp3)
      - 2 * x3 * d1 * d1 * d2 * y3 * dr13p_inv * dr23p_inv * dr123p_32
      * (d1 * d1 + d2 * d2 + 2 * dp3 * dp3)
      + 2 * r2_inv * d2 * dr13p_inv * dr23p_inv
      * (x1 * x1 * x3 * x3 - 2 * x1 * x3 * x3 * y1 + x3 * x3 * y1 * y1
         + 5 * x1 * x1 * x3 * y3 + x2 * x2 * x3 * y3
         + 2 * x3 * x3 * x3 * y3 - 10 * x1 * x3 * y1 * y3
         + 5 * x3 * y1 * y1 * y3 - 2 * x2 * x3 * y2 * y3
         + x3 * y2 * y2 * y3 + x1 * x1 * y3 * y3 + 4 * x3 * x3 * y3 * y3
         - 2 * x1 * y1 * y3 * y3 + y1 * y1 * y3 * y3
         + 2 * x3 * y3 * y3 * y3 - 4 * nu * d1 * d1 * dp3 * dp3
         + 4 * nu * nu * d1 * d1 * dp3 * dp3)
      + xLogy (3 - 4 * nu, r1 + x2 - y2)
      + xLogy (5 + 4 * nu * ((-3) + 2 * nu), r2 + x2 - y2);
    /// d/dy
    result (iy)
      = - ((-3) + 4 * nu) * r1_inv * r1xy1_inv * d2 * d2
      + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * r2xy1_inv * d2 * d2
      - ((-3) + 4 * nu) * d1 * r1xy2_inv
      - ((-3) + 4 * nu) * r1_inv * d1 * d2 * r1xy2_inv
      + (5 + 4 * nu * ((-3) + 2 * nu)) * d1 * r2xy2_inv
      + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d1 * d2 * r2xy2_inv
      + 2 * ((-1) + 2 * nu) * r1 * d1 * dr13_inv * dr23_inv * d3 * d3
      - 2 * ((-1) + 2 * nu) * r1_inv * d1 * d2 * d2 * dr13_inv * dr23_inv
      * d3 * d3
      - 2 * (1 - 2 * nu) * (1 - 2 * nu) * r2 * d1 * dp3 * dp3 * dr13p_inv
      * dr23p_inv
      - 4 * r2_inv * x3 * d1 * d2 * d2 * y3 * dr13p_inv * dr23p_inv
      * dr23p_inv * (d1 * d1 + d2 * d2 + 2 * dp3 * dp3)
      - 2 * x3 * d1 * d2 * d2 * y3 * dr13p_inv * dr23p_inv * dr123p_32
      * (d1 * d1 + d2 * d2 + 2 * dp3 * dp3)
      + 2 * r2_inv * d1 * dr13p_inv * dr23p_inv
      * (x2 * x2 * x3 * x3 - 2 * x2 * x3 * x3 * y2 + x3 * x3 * y2 * y2
         + x1 * x1 * x3 * y3 + 5 * x2 * x2 * x3 * y3
         + 2 * x3 * x3 * x3 * y3 - 2 * x1 * x3 * y1 * y3
         + x3 * y1 * y1 * y3 - 10 * x2 * x3 * y2 * y3
         + 5 * x3 * y2 * y2 * y3 + x2 * x2 * y3 * y3
         + 4 * x3 * x3 * y3 * y3 - 2 * x2 * y2 * y3 * y3
         + y2 * y2 * y3 * y3 + 2 * x3 * y3 * y3 * y3
         - 4 * nu * d2 * d2 * dp3 * dp3
         + 4 * nu * nu * d2 * d2 * dp3 * dp3)
      + xLogy (3 - 4 * nu, r1 + x1 - y1)
      + xLogy (5 + 4 * nu * ((-3) + 2 * nu), r2 + x1 - y1);
    /// d/dz
    result (2)
      = - ((-3) + 4 * nu) * r1_inv * r1xy1_inv * d2 * d3
      - ((-3) + 4 * nu) * r1_inv * d1 * r1xy2_inv * d3
      - 2 * ((-1) + 2 * nu) * r1 * d1 * d2 * dr13_inv * dr23_inv * d3
      - 2 * ((-1) + 2 * nu) * r1_inv * d1 * d2 * dr13_inv * dr23_inv * d3
      * d3 * d3
      + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * r2xy1_inv * d2 * dp3
      + (5 + 4 * nu * ((-3) + 2 * nu)) * r2_inv * d1 * r2xy2_inv * dp3
      + 2 * (1 - 2 * nu) * (1 - 2 * nu) * r2 * d1 * d2 * dp3 * dr13p_inv
      * dr23p_inv
      - 4 * r2_inv * x3 * d1 * d2 * y3 * dp3 * dr13p_inv * dr23p_inv
      * dr23p_inv * (d1 * d1 + d2 * d2 + 2 * dp3 * dp3)
      - 4 * r2_inv * x3 * d1 * d2 * y3 * dp3 * dr13p_inv * dr13p_inv
      * dr23p_inv * (d1 * d1 + d2 * d2 + 2 * dp3 * dp3)
      - 2 * x3 * d1 * d2 * y3 * dp3 * dr13p_inv * dr23p_inv * dr123p_32
      * (d1 * d1 + d2 * d2 + 2 * dp3 * dp3)
      + 2 * r2_inv * d1 * d2 * dr13p_inv * dr23p_inv
      * (x3 * x3 * x3 + (x1 * x1 + x2 * x2) * y3 + 9 * x3 * x3 * y3
         - 2 * x1 * y1 * y3 + y1 * y1 * y3 - 2 * x2 * y2 * y3
         + y2 * y2 * y3 + 11 * x3 * y3 * y3 + 3 * y3 * y3 * y3
         - 4 * nu * dp3 * dp3 * dp3 + 4 * nu * nu * dp3 * dp3 * dp3)
      + ((-2) + 4 * nu) * atan2 (r1 * (-x3 + y3), d1 * d2)
      + 2 * (1 - 2 * nu) * (1 - 2 * nu) * atan2 (r2 * dp3, d1 * d2);
    return result;
  }
}
