#include "Integrals.hxx"
#include "../../Strain_Volume.hxx"

namespace okada
{
  FTensor::Tensor2<double,3,3>
  Strain_Volume::dIU(const FTensor::Tensor1<double,3> &xyz,
                     const FTensor::Tensor1<double,3> &y) const
  {
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    FTensor::Index<'k',3> k;
    FTensor::Tensor2<double,3,3> result;
    std::vector<FTensor::Tensor3<double,3,3,3> >
      integrals(Integrals(xyz,y,nu).dJ());
    for(int l=0;l<3;++l)
      for(int m=0;m<3;++m)
        {
          result(m,l)=stress(j,k)*integrals[l](m,j,k);
        }
    return result;
  }
}
