#include "../../Strain_Volume.hxx"

#include <stdexcept>

namespace okada
{
  Displacement
  Strain_Volume::displacement(const FTensor::Tensor1<double,3> &global_coord)
    const
  {
    Displacement result;
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    FTensor::Index<'k',3> k;
    FTensor::Index<'l',3> l;
    /// displacement is zero in the air.
    if(global_coord(2)<0)
      {
        result.first(i)=0;
        result.second(i,j)=0;
        return result;
      }
    /// Only subtract the x and y components of the origin.
    FTensor::Tensor1<double,3> offset(global_coord(0) - origin(0),
                                      global_coord(1) - origin(1),
                                      global_coord(2)), xyz;
    xyz(i)=offset(j)*rotation_strike(i,j);

    /// The convention here is that thickness (shape(2) is in the z direction)
    FTensor::Tensor1<double,3> L_q3_p_W(shape(0),shape(1)/2,origin(2) + shape(2)),
      L_q3_m_W(shape(0),-shape(1)/2,origin(2) + shape(2)),
      L_q3_p(shape(0),shape(1)/2,origin(2)),
      L_q3_m(shape(0),-shape(1)/2,origin(2)),
      q3_p_W(0,shape(1)/2,origin(2) + shape(2)),
      q3_m_W(0,-shape(1)/2,origin(2) + shape(2)),
      q3_p(0,shape(1)/2,origin(2)),
      q3_m(0,-shape(1)/2,origin(2));
    result.first(i)=scale*(IU(xyz,L_q3_p_W)(j) - IU(xyz,L_q3_m_W)(j)
                           + IU(xyz,L_q3_m)(j) - IU(xyz,L_q3_p)(j)
                           - IU(xyz,q3_p_W)(j) + IU(xyz,q3_m_W)(j)
                           - IU(xyz,q3_m)(j) + IU(xyz,q3_p)(j))
      *rotation_strike(j,i);

    FTensor::Tensor2<double,3,3> derivatives;
    derivatives(i,j)=scale*(dIU(xyz,L_q3_p_W)(i,j) - dIU(xyz,L_q3_m_W)(i,j)
                            + dIU(xyz,L_q3_m)(i,j) - dIU(xyz,L_q3_p)(i,j)
                            - dIU(xyz,q3_p_W)(i,j) + dIU(xyz,q3_m_W)(i,j)
                            - dIU(xyz,q3_m)(i,j) + dIU(xyz,q3_p)(i,j));
    FTensor::Tensor2_symmetric<double,3> rotated_strain;
    rotated_strain(i,j)=(derivatives(i,j) || derivatives(j,i))/2;
    
    result.second(i,j)=(rotated_strain(k,l)*rotation_strike(k,i))
      ^rotation_strike(l,j);

    /// Subtract the inelastic applied strain
    if (is_inside_strain_volume (xyz))
      { result.second(i,j)-=rotated_applied_strain(i,j); }
    return result;
  }
}
