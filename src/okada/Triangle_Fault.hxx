#pragma once

#include "Displacement.hxx"

namespace okada
{
  class Triangle_Fault
  {
  public:
    FTensor::Tensor1<double,3> origin, origin_image, u, u_image, v, v_image,
      jump, b;
    FTensor::Tensor2<double,3,3> At, At_image;
    double nu;
  
    Triangle_Fault(const FTensor::Tensor1<double,3> &Origin,
                   const FTensor::Tensor1<double,3> &U,
                   const FTensor::Tensor1<double,3> &V,
                   const FTensor::Tensor1<double,3> &Jump,
                   const double &lambda, const double &mu);
    Displacement displacement(const FTensor::Tensor1<double,3> &coord);
  };
}
