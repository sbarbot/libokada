#include "../Triangle_Fault.hxx"
#include "../invert_z.hxx"
#include "../cross.hxx"

namespace
{
  void compute_transform(const FTensor::Tensor1<double,3> &Vnorm,
                         const FTensor::Tensor1<double,3> &Vstrike,
                         FTensor::Tensor2<double,3,3> &At)
  {
    FTensor::Index<'i',3> i;
    FTensor::Tensor1<double,3> Vdip(okada::cross(Vnorm,Vstrike));
    FTensor::Number<0> N0;
    FTensor::Number<1> N1;
    FTensor::Number<2> N2;
    At(i,N0)=Vnorm(i);
    At(i,N1)=Vstrike(i);
    At(i,N2)=Vdip(i);
  }
}

namespace okada
{
  Triangle_Fault::Triangle_Fault(const FTensor::Tensor1<double,3> &Origin,
                                 const FTensor::Tensor1<double,3> &U,
                                 const FTensor::Tensor1<double,3> &V,
                                 const FTensor::Tensor1<double,3> &Jump,
                                 const double &lambda, const double &mu) :
    origin(invert_z(Origin)), origin_image(Origin), u(invert_z(U)), u_image(U),
    v(invert_z(V)), v_image(V), jump(invert_z(Jump)), nu(lambda/(2*(lambda+mu)))
  {
    FTensor::Tensor1<double,3> Vnorm(cross(u,v).normalize()),
      Vnorm_image(cross(u_image,v_image).normalize());

    FTensor::Tensor1<double,3> eY(0,1,0), eZ(0,0,1);
    FTensor::Tensor1<double,3> Vstrike(cross(eZ,Vnorm).normalize());
    FTensor::Tensor1<double,3> Vstrike_image(cross(eZ,Vnorm_image).normalize());

    /// Special case when the fault is horizontal.
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    if(Vstrike.l2()==0)
      { Vstrike(i)=eY(i); }
    if(Vstrike_image.l2()==0)
      { Vstrike_image(i)=-eY(i); }
  
    compute_transform(Vnorm,Vstrike,At);
    compute_transform(Vnorm_image,Vstrike_image,At_image);

    b(i)=At(j,i)*jump(j);
  }
}
