/// Calculates the "incomplete" displacements (without the Burgers'
/// function contribution) associated with an angular dislocation in
/// an elastic full-space.

#include "../../../../Displacement.hxx"

#include <stdexcept>
/// For M_PI
#define _USE_MATH_DEFINES
#include <cmath>

namespace okada
{
  Displacement
  angular_dislocation_unrotated(const FTensor::Tensor1<double,3> &xyz,
                                const double &alpha,
                                const FTensor::Tensor1<double,3> &b1,
                                const double &nu)
  {
    const double x(xyz(0)), y(xyz(1)), z(xyz(2)),
      cosA(cos(alpha)), sinA(sin(alpha)), eta(y*cosA-z*sinA),
      zeta(y*sinA+z*cosA);

    const double x2(x*x), y2(y*y), z2(z*z), r2(x2+y2+z2), r(sqrt(r2)),
      r3(r*r2), rz(r*(r-z)), r2z2(r2*(r-z)*(r-z)), r3z(r3*(r-z)), W(zeta-r),
      W2(W*W), Wr(W*r), W2r(W2*r), Wr3(W*r3), W2r2(W2*r2), C((r*cosA-z)/Wr),
      S((r*sinA-y)/Wr);

    /// Avoid complex results for the logarithmic terms.  Unfortunately,
    /// this just makes them diverge :(
    if(zeta>r || z>r)
      { throw std::runtime_error("Unable to compute solution "
                                 "at this positions"); }
    const FTensor::Tensor2<double,3,3>
      uvw(x*y/r/(r-z)-x*eta/r/(r-zeta),
          eta*sinA/(r-zeta) - y*eta/r/(r-zeta) + y*y/r/(r-z) + (1-2*nu)*(cosA*log(r-zeta)-log(r-z)),
          eta*cosA/(r-zeta) - y/r - eta*z/r/(r-zeta) - (1-2*nu)*sinA*log(r-zeta),
          x*x*cosA/r/(r-zeta) - x*x/r/(r-z) - (1-2*nu)*(cosA*log(r-zeta)-log(r-z)),
          x*(y*cosA/r/(r-zeta) - sinA*cosA/(r-zeta) - y/r/(r-z)),
          x*(z*cosA/r/(r-zeta) - cosA*cosA/(r-zeta) + 1/r),
          sinA*((1-2*nu)*log(r-zeta) - x*x/r/(r-zeta)),
          x*sinA*(sinA/(r-zeta) - y/r/(r-zeta)),
          x*sinA*(cosA/(r-zeta) - z/r/(r-zeta)));
    
    Displacement result;
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    result.first(i)=(b1(j)*uvw(j,i))/(8*M_PI*(1-nu));

    /// Partial derivatives of the Burgers' function
    const FTensor::Tensor1<double,3> dphi((eta/r/(r-zeta)-y/r/(r-z))/4/M_PI,
                                          (x/r/(r-z)-cosA*x/r/(r-zeta))/4/M_PI,
                                          (sinA*x/r/(r-zeta))/4/M_PI);
    result.second(0,0) = 
      b1(0)*(eta/Wr+eta*x2/W2r2-eta*x2/Wr3+y/rz - x2*y/r2z2-x2*y/r3z)-
      b1(1)*x*(((2*nu+1)/Wr+x2/W2r2-x2/Wr3)*cosA + (2*nu+1)/rz-x2/r2z2-x2/r3z)+
      b1(2)*x*sinA*((2*nu+1)/Wr+x2/W2r2-x2/Wr3);

    result.second(1,1) = 
      b1(0)*((1/Wr+S*S-y2/Wr3)*eta+(2*nu+1)*y/rz-y*y*y/r2z2 - y*y*y/r3z-2*nu*cosA*S)-
      b1(1)*x*(1/rz-y2/r2z2-y2/r3z + (1/Wr+S*S-y2/Wr3)*cosA)+
      b1(2)*x*sinA*(1/Wr+S*S-y2/Wr3);

    result.second(2,2) = 
      b1(0)*(eta/W/r+eta*C*C-eta*z2/Wr3+y*z/r3 + 2*nu*sinA*C)-
      b1(1)*x*((1/Wr+C*C-z2/Wr3)*cosA+z/r3)+
      b1(2)*x*sinA*(1/Wr+C*C-z2/Wr3);
    
    result.second(0,1) =
      -b1(0)*(x*y2/r2z2-nu*x/rz+x*y2/r3z-nu*x*cosA/Wr + eta*x*S/Wr+eta*x*y/Wr3)+
      b1(1)*(x2*y/r2z2-nu*y/rz+x2*y/r3z+nu*cosA*S + x2*y*cosA/Wr3+x2*cosA*S/Wr)-
      b1(2)*sinA*(nu*S+x2*S/Wr+x2*y/Wr3);

    result.second(0,2) = 
      -b1(0)*(-x*y/r3+nu*x*sinA/Wr+eta*x*C/Wr + eta*x*z/Wr3)+
      b1(1)*(-x2/r3+nu/r+nu*cosA*C+x2*z*cosA/Wr3 + x2*cosA*C/Wr)-
      b1(2)*sinA*(nu*C+x2*C/Wr+x2*z/Wr3);

    result.second(1,2) = 
      b1(0)*(y2/r3-nu/r-nu*cosA*C+nu*sinA*S+eta*sinA*cosA/W2 - eta*(y*cosA+z*sinA)/W2r+eta*y*z/W2r2-eta*y*z/Wr3)-
      b1(1)*x*(y/r3+sinA*cosA*cosA/W2-cosA*(y*cosA+z*sinA)/W2r+y*z*cosA/W2r2-y*z*cosA/Wr3)-
      b1(2)*x*sinA*(y*z/Wr3-sinA*cosA/W2+(y*cosA+z*sinA)/W2r-y*z/W2r2);

    result.second(i,j)=result.second(i,j)/(8*M_PI*(1-nu))
      + (b1(i)*dphi(j) || b1(j)*dphi(i))/2;

    return result;
  }
}
