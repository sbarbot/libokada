#include "../../../Displacement.hxx"

/// For M_PI
#define _USE_MATH_DEFINES
#include <cmath>

namespace okada
{
  Displacement
  angular_dislocation(const FTensor::Tensor1<double,3> &xyz,
                      const double &alpha,
                      const FTensor::Tensor1<double,3> &b,
                      const double &nu,
                      const FTensor::Tensor1<double,3> &TriVertex,
                      const FTensor::Tensor1<double,3> &SideVec,
                      const double &sign);


  int trimode_finder(const double &x,
                     const double &y,
                     const double &z,
                     const FTensor::Tensor1<double,3> &p1,
                     const FTensor::Tensor1<double,3> &p2,
                     const FTensor::Tensor1<double,3> &p3);

  Displacement
  dislocation_displacement
  (const FTensor::Tensor1<double,3> &coord,
   const FTensor::Tensor1<double,3> &jump_vector,
   const FTensor::Tensor2<double,3,3> &transform_matrix,
   const FTensor::Tensor1<double,3> &origin_local,
   const FTensor::Tensor1<double,3> &u_local,
   const FTensor::Tensor1<double,3> &v_local,
   const double &nu)
  {
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    FTensor::Index<'k',3> k;
    FTensor::Index<'l',3> l;
    FTensor::Tensor1<double,3> transformed;
    transformed(i)=transform_matrix(j,i)*(coord(j)-(origin_local(j)+u_local(j)));

    /// Calculate the unit vectors along TD sides in TDCS
    FTensor::Tensor1<double,3> p1, p2(0,0,0), p3;
    p1(i)= -transform_matrix(j,i)*u_local(j);
    p3(i)=  transform_matrix(j,i)*(v_local(j)-u_local(j));

    /// Calculate the unit vectors along TD sides in TDCS.

    FTensor::Tensor1<double,3> e12, e13, e23;
    e12(i)=p2(i)-p1(i);
    e12.normalize();
    e13(i)=p3(i)-p1(i);
    e13.normalize();
    e23(i)=p3(i)-p2(i);
    e23.normalize();

    /// Calculate the TD angles.
    const double A(acos(e12(i)*e13(i))), B(acos(-e12(i)*e23(i))),
      C(acos(e23(i)*e13(i)));

    int sign(trimode_finder(transformed(1),transformed(2),
                            transformed(0),p1,p2,p3));

    Displacement uvw_13(angular_dislocation(transformed,A,jump_vector,nu,p1,
                                            e13,-sign)),
      uvw_12(angular_dislocation(transformed,B,jump_vector,nu,p2,e12,sign)),
      uvw_23(angular_dislocation(transformed,C,jump_vector,nu,p3,e23,sign));

    Displacement uvw;
    uvw.first(i)=uvw_13.first(i) + uvw_12.first(i) + uvw_23.first(i);
    uvw.second(i,j)=uvw_13.second(i,j) + uvw_12.second(i,j) + uvw_23.second(i,j);

    FTensor::Tensor1<double,3>
      a(-transformed(0),p1(1)-transformed(1),p1(2)-transformed(2)),
      b(-transformed(0),-transformed(1),-transformed(2)),
      c(-transformed(0),p3(1)-transformed(1),p3(2)-transformed(2));
    const double na(a.l2()), nb(b.l2()), nc(c.l2());

    const double
      Fi(-2*atan2((a(0)*(b(1)*c(2)-b(2)*c(1))-
                   a(1)*(b(0)*c(2)-b(2)*c(0))+
                   a(2)*(b(0)*c(1)-b(1)*c(0))),
                  (na*nb*nc+a(i)*b(i)*nc+a(i)*c(i)*nb+b(i)*c(i)*na))/4/M_PI);

    FTensor::Tensor1<double,3> uvw_offset;
    uvw_offset(i)=jump_vector(i)*Fi + uvw.first(i);
    Displacement result;
    result.first(i)=transform_matrix(i,j)*uvw_offset(j);
    result.second(i,j)=(transform_matrix(j,k)*uvw.second(k,l))
      ^transform_matrix(i,l);
    return result;
  }
}
