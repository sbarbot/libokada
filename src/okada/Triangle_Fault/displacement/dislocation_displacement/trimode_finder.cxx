#include <FTensor.hpp>
#include <stdexcept>

namespace okada
{
  // C++ 11 will allow me to use proper enum class.
  enum Trimode {Trimode_Positive, Trimode_Negative, Trimode_Zero};
  
  int trimode_finder(const double &x,
                     const double &y,
                     const double &z,
                     const FTensor::Tensor1<double,3> &p1,
                     const FTensor::Tensor1<double,3> &p2,
                     const FTensor::Tensor1<double,3> &p3)
  {
    /// trimode_finder calculates the normalized barycentric coordinates
    /// of the points with respect to the TD vertices and specifies the
    /// appropriate artefact-free configuration of the angular
    /// dislocations for the calculations. The input matrices x, y and z
    /// share the same size and correspond to the y, z and x coordinates
    /// in the TDCS, respectively. The y and z components of p1, p2 and
    /// p3 are two-component matrices representing the y and z
    /// coordinates of the TD vertices in the TDCS, respectively.  The
    /// components of the output (trimode) corresponding to each
    /// calculation points, are 1 for the first configuration, -1 for
    /// the second configuration and 0 for the calculation point that
    /// lie on the TD sides.

    const double a = ((p2(2)-p3(2))*(x-p3(1))+(p3(1)-p2(1))*(y-p3(2)))
      /((p2(2)-p3(2))*(p1(1)-p3(1))+(p3(1)-p2(1))*(p1(2)-p3(2)));
    const double b = ((p3(2)-p1(2))*(x-p3(1))+(p1(1)-p3(1))*(y-p3(2)))
      /((p2(2)-p3(2))*(p1(1)-p3(1))+(p3(1)-p2(1))*(p1(2)-p3(2)));
    const double c = 1-a-b;

    Trimode result=Trimode_Positive;
    if((a<=0 && b>c && c>a)
       || (b<=0 && c>a && a>b)
       || (c<=0 && a>b && b>c))
      { result=Trimode_Negative; }
    if((a==0 && b>=0 && c>=0)
       || (a>=0 && b==0 && c>=0)
       || (a>=0 && b>=0 && c==0))
      { result=Trimode_Zero; }
    if(result==Trimode_Zero && z!=0)
      { result=Trimode_Positive; }
    if(result==Trimode_Zero)
      throw std::runtime_error("Unable to compute solution "
                               "at this position");
    return (result==Trimode_Positive ? 1 : -1);
  }
}
