#include "../../invert_z.hxx"
#include "../../Triangle_Fault.hxx"

namespace okada
{
  Displacement
  dislocation_displacement
  (const FTensor::Tensor1<double,3> &coord,
   const FTensor::Tensor1<double,3> &jump_vector,
   const FTensor::Tensor2<double,3,3> &transform_matrix,
   const FTensor::Tensor1<double,3> &origin_local,
   const FTensor::Tensor1<double,3> &u_local,
   const FTensor::Tensor1<double,3> &v_local,
   const double &nu);

  Displacement
  harmonic_displacement
  (const FTensor::Tensor1<double,3> &coord,
   const FTensor::Tensor1<double,3> &jump_vector,
   const FTensor::Tensor1<double,3> &origin_local,
   const FTensor::Tensor1<double,3> &u_local,
   const FTensor::Tensor1<double,3> &v_local,
   const double &nu);

  Displacement Triangle_Fault::displacement
  (const FTensor::Tensor1<double,3> &inverted_coord)
  {
    const FTensor::Tensor1<double,3> coord(invert_z(inverted_coord));
    Displacement dislocation(dislocation_displacement(coord,b,At,origin,u,v,nu)),
      mirror(dislocation_displacement(coord,b,At_image,origin_image,u_image,
                                      v_image,nu)),
      harmonic(harmonic_displacement(coord,jump,origin,u,v,nu));

    if(origin(2)==0 && u(2)==0 && v(2)==0)
      {
        mirror.first(2)=-mirror.first(2);
        mirror.second(0,2)=-mirror.second(0,2);
        mirror.second(1,2)=-mirror.second(1,2);
      }
        
    Displacement result;
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;

    result.first(i)=dislocation.first(i)+harmonic.first(i)+mirror.first(i);
    result.second(i,j)=dislocation.second(i,j)+harmonic.second(i,j)
      +mirror.second(i,j);

    /// Reverse the results if the fault corner is at the origin?  This
    /// was in the original matlab.
    if(origin(2)==0 && u(2)==0 && v(2)==0)
      result.first(i)=-result.first(i);

    result.first = invert_z(result.first);
    result.second = invert_z(result.second);
    return result;
  }
}
