/// Calculates the free surface correction to displacements associated
/// with angular dislocation pair on each TD side.

#include "../../../../cross.hxx"
#include "../../../../Displacement.hxx"

#include <FTensor.hpp>
#include <limits>
/// For M_PI
#define _USE_MATH_DEFINES
#include <cmath>

namespace okada
{
  FTensor::Tensor1<double,3>
  harmonic_correction_displacement(const FTensor::Tensor1<double,3> &y,
                                   const double &beta,
                                   const FTensor::Tensor1<double,3> &jump,
                                   const double &nu,
                                   const double &a);
  FTensor::Tensor2_symmetric<double,3>
  harmonic_correction_strain(const FTensor::Tensor1<double,3> &y,
                             const double &beta,
                             const FTensor::Tensor1<double,3> &jump,
                             const double &nu,
                             const double &a);

  Displacement
  free_surface_correction(const FTensor::Tensor1<double,3> &coord,
                          const FTensor::Tensor1<double,3> &jump,
                          const FTensor::Tensor1<double,3> &PA,
                          const FTensor::Tensor1<double,3> &PB,
                          const double &nu)
  {
    /// Calculate TD side vector and the angle of the angular dislocation pair
    FTensor::Tensor1<double,3> SideVec, eZ(0,0,1);
    FTensor::Index<'i',3> i;
    FTensor::Index<'j',3> j;
    FTensor::Index<'k',3> k;
    FTensor::Index<'l',3> l;
    SideVec(i)=PB(i)-PA(i);
    const double beta(std::acos(-SideVec(i)*eZ(i)/SideVec.l2()));

    Displacement result;
    result.first(i)=0;
    result.second(i,j)=0;

    if (!(std::abs(beta)<std::numeric_limits<double>::epsilon()
          || std::abs(M_PI-beta)<std::numeric_limits<double>::epsilon()))
      {
        FTensor::Tensor1<double,3> ey1(SideVec(0),SideVec(1),0), ey3;
        ey1.normalize();
        ey3(i)=-eZ(i);
        FTensor::Tensor1<double,3> ey2(cross(ey3,ey1));
        FTensor::Tensor2<double,3,3> A; /// Transformation matrix
        FTensor::Number<0> N0;
        FTensor::Number<1> N1;
        FTensor::Number<2> N2;
        A(i,N0)=ey1(i);
        A(i,N1)=ey2(i);
        A(i,N2)=ey3(i);
    
        /// Transform coordinates from EFCS to the first ADCS
        FTensor::Tensor1<double,3> yA;
        yA(i)=A(j,i)*(coord(j)-PA(j));
        /// Transform coordinates from EFCS to the second ADCS
        FTensor::Tensor1<double,3> yAB;
        yAB(i)=A(j,i)*SideVec(j);
        FTensor::Tensor1<double,3> yB;
        yB(i)=yA(i)-yAB(i);
    
        /// Transform slip vector components from EFCS to ADCS
        FTensor::Tensor1<double,3> b;
        b(i)=A(j,i)*jump(j);
    
        /// Determine the best arteact-free configuration for the calculation
        /// points near the free furface
        /// Configuration I or II
        const bool is_configuration_I(beta*yA(0)>=0);
        const double beta_displacement(is_configuration_I ? -M_PI+beta : beta),
          beta_strain(is_configuration_I ? M_PI-beta: beta);

        /// Calculate total Free Surface Correction to displacements in
        /// ADCS and transform to EFCS.
        result.first(i)=A(i,j)
          *(harmonic_correction_displacement(yB,beta_displacement,b,nu,-PB(2))(j)
            - harmonic_correction_displacement(yA,beta_displacement,b,nu,
                                               -PA(2))(j));

        if(is_configuration_I)
          {
            yA(0)*=-1;
            yA(1)*=-1;
            yB(0)*=-1;
            yB(1)*=-1;
            b(0)*=-1;
            b(1)*=-1;
          }
        FTensor::Tensor2_symmetric<double,3> harmonic_corrections;
        harmonic_corrections(i,j)=
          harmonic_correction_strain(yB,beta_strain,b,nu,-PB(2))(i,j)
          - harmonic_correction_strain(yA,beta_strain,b,nu,-PA(2))(i,j);
        if(is_configuration_I)
          {
            harmonic_corrections(0,2)*=-1;
            harmonic_corrections(1,2)*=-1;
          }
        result.second(i,j)=A(i,k)*harmonic_corrections(k,l)^A(l,j);
      }
    return result;
  }
}
