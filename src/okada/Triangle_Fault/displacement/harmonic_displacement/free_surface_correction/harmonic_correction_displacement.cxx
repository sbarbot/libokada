/// Calculates the harmonic function contribution to the displacements
/// associated with an angular dislocation in an elastic half-space.

#include <FTensor.hpp>
/// For M_PI
#define _USE_MATH_DEFINES
#include <cmath>

namespace okada
{
  FTensor::Tensor1<double,3>
  harmonic_correction_displacement(const FTensor::Tensor1<double,3> &y,
                                   const double &beta,
                                   const FTensor::Tensor1<double,3> &jump,
                                   const double &nu,
                                   const double &a)
  {
    double sinB(std::sin(beta)), cosB(std::cos(beta)), cotB(1/std::tan(beta)),
      yzb(y(2)+2*a), zxb(y(0)*cosB+yzb*sinB), zzb(-y(0)*sinB+yzb*cosB),
      r2b(y(0)*y(0)+y(1)*y(1)+yzb*yzb), rb(sqrt(r2b)), r3b(rb*r2b);

    /// The Burgers' function
    double Fib(2*std::atan(-y(1)/(-(rb+yzb)/std::tan(beta/2)+y(0))));

    double
      v1cb1 = jump(0)/4/M_PI/(1-nu)
      *(-2*(1-nu)*(1-2*nu)*Fib*cotB*cotB+(1-2*nu)*y(1)/
        (rb+yzb)*((1-2*nu-a/rb)*cotB-y(0)/(rb+yzb)*(nu+a/rb))+(1-2*nu)*
        y(1)*cosB*cotB/(rb+zzb)*(cosB+a/rb)+a*y(1)*(yzb-a)*cotB/r3b+y(1)*
        (yzb-a)/(rb*(rb+yzb))*(-(1-2*nu)*cotB+y(0)/(rb+yzb)*(2*nu+a/rb)+
                               a*y(0)/r2b)+y(1)*(yzb-a)/(rb*(rb+zzb))
        *(cosB/(rb+zzb)*((rb*cosB+yzb)*((1-2*nu)*cosB-a/rb)*cotB
                         +2*(1-nu)*(rb*sinB-y(0))*cosB)-
          a*yzb*cosB*cotB/r2b)),

      v2cb1 = jump(0)/4/M_PI/(1-nu)
      *((1-2*nu)*((2*(1-nu)*cotB*cotB-nu)*log(rb+yzb)
                  -(2*(1-nu)*cotB*cotB+1-2*nu)*cosB*log(rb+zzb))-(1-2*nu)/(rb+yzb)
        *(y(0)*cotB*(1-2*nu-a/rb)+nu*yzb-a+y(1)*y(1)/(rb+yzb)*(nu+a/rb))
        -(1-2*nu)*zxb*cotB/(rb+zzb)*(cosB+a/rb)-a*y(0)*(yzb-a)*cotB/r3b+
        (yzb-a)/(rb+yzb)*(-2*nu+1/rb*((1-2*nu)*y(0)*cotB-a)+y(1)*y(1)
                          /(rb*(rb+yzb))*(2*nu+a/rb)+a*y(1)*y(1)/r3b)
        +(yzb-a)/(rb+zzb)
        *(cosB*cosB-1/rb*((1-2*nu)*zxb*cotB+a*cosB)+a*yzb*zxb*cotB/r3b-1
          /(rb*(rb+zzb))*(y(1)*y(1)*cosB*cosB-a*zxb*cotB/rb*(rb*cosB+yzb)))),

      v3cb1 = jump(0)/4/M_PI/(1-nu)
      *(2*(1-nu)*(((1-2*nu)*Fib*cotB)+(y(1)/(rb+yzb)*(2*nu+a/rb))
                  -(y(1)*cosB/(rb+zzb)*(cosB+a/rb)))+y(1)*(yzb-a)
        /rb*(2*nu/(rb+yzb)+a/r2b)+y(1)*(yzb-a)*cosB/(rb*(rb+zzb))
        *(1-2*nu-(rb*cosB+yzb)/(rb+zzb)*(cosB+a/rb)-a*yzb/r2b)),
    
      v1cb2 = jump(1)/4/M_PI/(1-nu)
      *((1-2*nu)*((2*(1-nu)*cotB*cotB+nu)*log(rb+yzb)
                  -(2*(1-nu)*cotB*cotB+1)*cosB*log(rb+zzb))+(1-2*nu)/(rb+yzb)
        *(-(1-2*nu)*y(0)*cotB+nu*yzb-a+a*y(0)*cotB/rb+y(0)*y(0)/(rb+yzb)*(nu+a/rb))
        -(1-2*nu)*cotB/(rb+zzb)*(zxb*cosB-a*(rb*sinB-y(0))/(rb*cosB))-a*y(0)*
        (yzb-a)*cotB/r3b+(yzb-a)/(rb+yzb)
        *(2*nu+1/rb*((1-2*nu)*y(0)*cotB+a)-y(0)*y(0)/(rb*(rb+yzb))
          *(2*nu+a/rb)-a*y(0)*y(0)/r3b)+(yzb-a)*
        cotB/(rb+zzb)*(-cosB*sinB+a*y(0)*yzb/(r3b*cosB)+(rb*sinB-y(0))/
                       rb*(2*(1-nu)*cosB-(rb*cosB+yzb)/(rb+zzb)*(1+a/(rb*cosB))))),
      v2cb2 = jump(1)/4/M_PI/(1-nu)
      *(2*(1-nu)*(1-2*nu)*Fib*cotB*cotB+(1-2*nu)*y(1)/
        (rb+yzb)*(-(1-2*nu-a/rb)*cotB+y(0)/(rb+yzb)*(nu+a/rb))-(1-2*nu)*
        y(1)*cotB/(rb+zzb)*(1+a/(rb*cosB))-a*y(1)*(yzb-a)*cotB/r3b+y(1)*
        (yzb-a)/(rb*(rb+yzb))*((1-2*nu)*cotB-2*nu*y(0)/(rb+yzb)-a*y(0)/rb*
                               (1/rb+1/(rb+yzb)))+y(1)*(yzb-a)*cotB/(rb*(rb+zzb))
        *(-2*(1-nu)*cosB+(rb*cosB+yzb)/(rb+zzb)*(1+a/(rb*cosB))+a*yzb/(r2b*cosB))),
      v3cb2 = jump(1)/4/M_PI/(1-nu)
      *(-2*(1-nu)*(1-2*nu)*cotB
        *(log(rb+yzb)-cosB*log(rb+zzb))-2*(1-nu)*y(0)/(rb+yzb)*(2*nu+a/rb)+2*(1-nu)*zxb
        /(rb+zzb)*(cosB+a/rb)+(yzb-a)/rb*((1-2*nu)*cotB-2*nu*y(0)/(rb+yzb)-a*
                                          y(0)/r2b)-(yzb-a)/(rb+zzb)
        *(cosB*sinB+(rb*cosB+yzb)*cotB/rb*(2*(1-nu)*cosB-(rb*cosB+yzb)/(rb+zzb))
          +a/rb*(sinB-yzb*zxb/r2b-zxb*(rb*cosB+yzb)/(rb*(rb+zzb))))),
      v1cb3 = jump(2)/4/M_PI/(1-nu)
      *((1-2*nu)*(y(1)/(rb+yzb)*(1+a/rb)-y(1)*cosB
                  /(rb+zzb)*(cosB+a/rb))-y(1)*(yzb-a)/rb*(a/r2b+1/(rb+yzb))+y(1)*
        (yzb-a)*cosB/(rb*(rb+zzb))*((rb*cosB+yzb)/(rb+zzb)*(cosB+a/rb)+a*yzb/r2b)),
      v2cb3 = jump(2)/4/M_PI/(1-nu)
      *((1-2*nu)*(-sinB*log(rb+zzb)-y(0)/(rb+yzb)
                  *(1+a/rb)+zxb/(rb+zzb)*(cosB+a/rb))+y(0)*(yzb-a)/rb
        *(a/r2b+1/(rb+yzb))-(yzb-a)/(rb+zzb)
        *(sinB*(cosB-a/rb)+zxb/rb*(1+a*yzb/r2b)-1/(rb*(rb+zzb))
          *(y(1)*y(1)*cosB*sinB-a*zxb/rb*(rb*cosB+yzb)))),
      v3cb3 = jump(2)/4/M_PI/(1-nu)
      *(2*(1-nu)*Fib+2*(1-nu)
        *(y(1)*sinB/(rb+zzb)*(cosB+a/rb))+y(1)*(yzb-a)*sinB/(rb*(rb+zzb))
        *(1+(rb*cosB+yzb)/(rb+zzb)*(cosB+a/rb)+a*yzb/r2b));
    return FTensor::Tensor1<double,3>(v1cb1+v1cb2+v1cb3,v2cb1+v2cb2+v2cb3,
                                      v3cb1+v3cb2+v3cb3);
  }
}
