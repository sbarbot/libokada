/// AngDisStrainFSC calculates the harmonic function contribution to
/// the strains associated with an angular dislocation in an elastic
/// half-space.

#include <FTensor.hpp>
/// For M_PI
#define _USE_MATH_DEFINES
#include <cmath>

namespace okada
{
  FTensor::Tensor2_symmetric<double,3>
  harmonic_correction_strain(const FTensor::Tensor1<double,3> &y,
                             const double &beta,
                             const FTensor::Tensor1<double,3> &jump,
                             const double &nu,
                             const double &a)
  {
    double sinB(std::sin(beta)), cosB(std::cos(beta)), cotB(1/std::tan(beta)),
      yzb(y(2)+2*a), zxb(y(0)*cosB+yzb*sinB), zzb(-y(0)*sinB+yzb*cosB),
      rb2(y(0)*y(0)+y(1)*y(1)+yzb*yzb), rb(std::sqrt(rb2)), rb3(rb*rb2);

    double W1(rb*cosB+yzb), W2(cosB+a/rb), W3(cosB+yzb/rb), W4(nu+a/rb),
      W5(2*nu+a/rb), W6(rb+yzb), W7(rb+zzb), W8(y(2)+a), W9(1+a/rb/cosB),
      N1(1-2*nu);

    /// Partial derivatives of the Burgers' function
    double rFib_ry2(zxb/rb/(rb+zzb)-y(0)/rb/(rb+yzb)), /// y2 = x in ADCS
      rFib_ry1(y(1)/rb/(rb+yzb)-cosB*y(1)/rb/(rb+zzb)), /// y1 =y in ADCS
      rFib_ry3(-sinB*y(1)/rb/(rb+zzb)); /// y3 = z in ADCS

    FTensor::Tensor2_symmetric<double,3> result;
    result(0,0)
      = jump(0)*(0.25*((-2+2*nu)*N1*rFib_ry1*cotB*cotB-N1*y(1)/(W6*W6)*((1-W5)*cotB-
    y(0)/W6*W4)/rb*y(0)+N1*y(1)/W6*(a/rb3*y(0)*cotB-1/W6*W4+(y(0)*y(0))/
    (W6*W6)*W4/rb+(y(0)*y(0))/W6*a/rb3)-N1*y(1)*cosB*cotB/(W7*W7)*W2*(y(0)/
    rb-sinB)-N1*y(1)*cosB*cotB/W7*a/rb3*y(0)-3*a*y(1)*W8*cotB/(rb2*rb3)*
    y(0)-y(1)*W8/rb3/W6*(-N1*cotB+y(0)/W6*W5+a*y(0)/rb2)*y(0)-y(1)*W8/
    rb2/(W6*W6)*(-N1*cotB+y(0)/W6*W5+a*y(0)/rb2)*y(0)+y(1)*W8/rb/W6*
                 (1/W6*W5-(y(0)*y(0))/(W6*W6)*W5/rb-(y(0)*y(0))/W6*a/rb3+a/rb2-2*a*(y(0)*y(0))
    /(rb2*rb2))-y(1)*W8/rb3/W7*(cosB/W7*(W1*(N1*cosB-a/rb)*cotB+
    (2-2*nu)*(rb*sinB-y(0))*cosB)-a*yzb*cosB*cotB/rb2)*y(0)-y(1)*W8/rb/
    (W7*W7)*(cosB/W7*(W1*(N1*cosB-a/rb)*cotB+(2-2*nu)*(rb*sinB-y(0))*
    cosB)-a*yzb*cosB*cotB/rb2)*(y(0)/rb-sinB)+y(1)*W8/rb/W7
                 *(-cosB/(W7*W7)
                   * (W1*(N1*cosB-a/rb)*cotB+(2-2*nu)*(rb*sinB-y(0))*cosB)
                   * (y(0)/rb-sinB)+cosB/W7*(1/rb*cosB*y(0)*(N1*cosB-a/rb)*cotB+W1*a/rb3*y(0)*cotB+(2-2*nu)*(1/rb*sinB*y(0)-1)*cosB)+2*a*yzb*cosB*cotB/
    (rb2*rb2)*y(0)))/M_PI/(1-nu))+
    jump(1)*(0.25*(N1*(((2-2*nu)*cotB*cotB+nu)/rb*y(0)/W6-((2-2*nu)*cotB*cotB+1)*
    cosB*(y(0)/rb-sinB)/W7)-N1/(W6*W6)*(-N1*y(0)*cotB+nu*yzb-a+a*y(0)*
    cotB/rb+(y(0)*y(0))/W6*W4)/rb*y(0)+N1/W6*(-N1*cotB+a*cotB/rb-a*
    (y(0)*y(0))*cotB/rb3+2*y(0)/W6*W4-(y(0)*y(0)*y(0))/(W6*W6)*W4/rb-(y(0)*y(0)*y(0))/W6*a/
    rb3)+N1*cotB/(W7*W7)*(zxb*cosB-a*(rb*sinB-y(0))/rb/cosB)*(y(0)/
    rb-sinB)-N1*cotB/W7*((cosB*cosB)-a*(1/rb*sinB*y(0)-1)/rb/cosB+a*
    (rb*sinB-y(0))/rb3/cosB*y(0))-a*W8*cotB/rb3+3*a*(y(0)*y(0))*W8*
    cotB/(rb2*rb3)-W8/(W6*W6)*(2*nu+1/rb*(N1*y(0)*cotB+a)-(y(0)*y(0))/rb/W6*
    W5-a*(y(0)*y(0))/rb3)/rb*y(0)+W8/W6*(-1/rb3*(N1*y(0)*cotB+a)*y(0)+
    1/rb*N1*cotB-2*y(0)/rb/W6*W5+(y(0)*y(0)*y(0))/rb3/W6*W5+(y(0)*y(0)*y(0))/rb2/
    (W6*W6)*W5+(y(0)*y(0)*y(0))/(rb2*rb2)/W6*a-2*a/rb3*y(0)+3*a*(y(0)*y(0)*y(0))/(rb2*rb3))-W8*
    cotB/(W7*W7)*(-cosB*sinB+a*y(0)*yzb/rb3/cosB+(rb*sinB-y(0))/rb*
    ((2-2*nu)*cosB-W1/W7*W9))*(y(0)/rb-sinB)+W8*cotB/W7*(a*yzb/
    rb3/cosB-3*a*(y(0)*y(0))*yzb/(rb2*rb3)/cosB+(1/rb*sinB*y(0)-1)/rb*
    ((2-2*nu)*cosB-W1/W7*W9)-(rb*sinB-y(0))/rb3*((2-2*nu)*cosB-W1/
    W7*W9)*y(0)+(rb*sinB-y(0))/rb*(-1/rb*cosB*y(0)/W7*W9+W1/(W7*W7)*
    W9*(y(0)/rb-sinB)+W1/W7*a/rb3/cosB*y(0))))/M_PI/(1-nu))+
    jump(2)*(0.25*(N1*(-y(1)/(W6*W6)*(1+a/rb)/rb*y(0)-y(1)/W6*a/rb3*y(0)+y(1)*
    cosB/(W7*W7)*W2*(y(0)/rb-sinB)+y(1)*cosB/W7*a/rb3*y(0))+y(1)*W8/
    rb3*(a/rb2+1/W6)*y(0)-y(1)*W8/rb*(-2*a/(rb2*rb2)*y(0)-1/(W6*W6)/
    rb*y(0))-y(1)*W8*cosB/rb3/W7*(W1/W7*W2+a*yzb/rb2)*y(0)-y(1)*W8*
    cosB/rb/(W7*W7)*(W1/W7*W2+a*yzb/rb2)*(y(0)/rb-sinB)+y(1)*W8*
    cosB/rb/W7*(1/rb*cosB*y(0)/W7*W2-W1/(W7*W7)*W2*(y(0)/rb-sinB)-
    W1/W7*a/rb3*y(0)-2*a*yzb/(rb2*rb2)*y(0)))/M_PI/(1-nu));

    result(1,1) = jump(0)*(0.25*(N1*(((2-2*nu)*cotB*cotB-nu)/rb*y(1)/W6-((2-2*nu)*cotB*cotB+1-
                                                     2*nu)*cosB/rb*y(1)/W7)+N1/(W6*W6)*(y(0)*cotB*(1-W5)+nu*yzb-a+(y(1)*y(1))/W6*W4)/rb*y(1)-N1/W6*(a*y(0)*cotB/rb3*y(1)+2*y(1)/W6*W4-(y(1)*y(1)*y(1))/(W6*W6)*W4/rb-(y(1)*y(1)*y(1))/W6*a/rb3)+N1*zxb*cotB/(W7*W7)*W2/rb*
    y(1)+N1*zxb*cotB/W7*a/rb3*y(1)+3*a*y(1)*W8*cotB/(rb2*rb3)*y(0)-W8/
    (W6*W6)*(-2*nu+1/rb*(N1*y(0)*cotB-a)+(y(1)*y(1))/rb/W6*W5+a*(y(1)*y(1))/
    rb3)/rb*y(1)+W8/W6*(-1/rb3*(N1*y(0)*cotB-a)*y(1)+2*y(1)/rb/
    W6*W5-(y(1)*y(1)*y(1))/rb3/W6*W5-(y(1)*y(1)*y(1))/rb2/(W6*W6)*W5-(y(1)*y(1)*y(1))/(rb2*rb2)/W6*
    a+2*a/rb3*y(1)-3*a*(y(1)*y(1)*y(1))/(rb2*rb3))-W8/(W7*W7)*((cosB*cosB)-1/rb*(N1*
    zxb*cotB+a*cosB)+a*yzb*zxb*cotB/rb3-1/rb/W7*((y(1)*y(1))*(cosB*cosB)-
    a*zxb*cotB/rb*W1))/rb*y(1)+W8/W7*(1/rb3*(N1*zxb*cotB+a*
    cosB)*y(1)-3*a*yzb*zxb*cotB/(rb2*rb3)*y(1)+1/rb3/W7*((y(1)*y(1))*(cosB*cosB)-
    a*zxb*cotB/rb*W1)*y(1)+1/rb2/(W7*W7)*((y(1)*y(1))*(cosB*cosB)-a*zxb*cotB/
    rb*W1)*y(1)-1/rb/W7*(2*y(1)*(cosB*cosB)+a*zxb*cotB/rb3*W1*y(1)-a*
    zxb*cotB/rb2*cosB*y(1))))/M_PI/(1-nu))+
      jump(1)*(0.25*((2-2*nu)*N1*rFib_ry2*cotB*cotB+N1/W6*((W5-1)*cotB+y(0)/W6*
    W4)-N1*(y(1)*y(1))/(W6*W6)*((W5-1)*cotB+y(0)/W6*W4)/rb+N1*y(1)/W6*(-a/
    rb3*y(1)*cotB-y(0)/(W6*W6)*W4/rb*y(1)-y(1)/W6*a/rb3*y(0))-N1*cotB/
    W7*W9+N1*(y(1)*y(1))*cotB/(W7*W7)*W9/rb+N1*(y(1)*y(1))*cotB/W7*a/rb3/
    cosB-a*W8*cotB/rb3+3*a*(y(1)*y(1))*W8*cotB/(rb2*rb3)+W8/rb/W6*(N1*
    cotB-2*nu*y(0)/W6-a*y(0)/rb*(1/rb+1/W6))-(y(1)*y(1))*W8/rb3/W6*
    (N1*cotB-2*nu*y(0)/W6-a*y(0)/rb*(1/rb+1/W6))-(y(1)*y(1))*W8/rb2/(W6*W6)*(N1*cotB-2*nu*y(0)/W6-a*y(0)/rb*(1/rb+1/W6))+y(1)*W8/rb/W6*
    (2*nu*y(0)/(W6*W6)/rb*y(1)+a*y(0)/rb3*(1/rb+1/W6)*y(1)-a*y(0)/rb*
    (-1/rb3*y(1)-1/(W6*W6)/rb*y(1)))+W8*cotB/rb/W7*((-2+2*nu)*cosB+
    W1/W7*W9+a*yzb/rb2/cosB)-(y(1)*y(1))*W8*cotB/rb3/W7*((-2+2*nu)*
    cosB+W1/W7*W9+a*yzb/rb2/cosB)-(y(1)*y(1))*W8*cotB/rb2/(W7*W7)*((-2+
    2*nu)*cosB+W1/W7*W9+a*yzb/rb2/cosB)+y(1)*W8*cotB/rb/W7*(1/
    rb*cosB*y(1)/W7*W9-W1/(W7*W7)*W9/rb*y(1)-W1/W7*a/rb3/cosB*y(1)-
    2*a*yzb/(rb2*rb2)/cosB*y(1)))/M_PI/(1-nu))+
    jump(2)*(0.25*(N1*(-sinB/rb*y(1)/W7+y(1)/(W6*W6)*(1+a/rb)/rb*y(0)+y(1)/W6*
    a/rb3*y(0)-zxb/(W7*W7)*W2/rb*y(1)-zxb/W7*a/rb3*y(1))-y(1)*W8/
    rb3*(a/rb2+1/W6)*y(0)+y(0)*W8/rb*(-2*a/(rb2*rb2)*y(1)-1/(W6*W6)/
    rb*y(1))+W8/(W7*W7)*(sinB*(cosB-a/rb)+zxb/rb*(1+a*yzb/rb2)-1/
    rb/W7*((y(1)*y(1))*cosB*sinB-a*zxb/rb*W1))/rb*y(1)-W8/W7*(sinB*a/
    rb3*y(1)-zxb/rb3*(1+a*yzb/rb2)*y(1)-2*zxb/(rb2*rb3)*a*yzb*y(1)+
    1/rb3/W7*((y(1)*y(1))*cosB*sinB-a*zxb/rb*W1)*y(1)+1/rb2/(W7*W7)*
    ((y(1)*y(1))*cosB*sinB-a*zxb/rb*W1)*y(1)-1/rb/W7*(2*y(1)*cosB*sinB+a*
    zxb/rb3*W1*y(1)-a*zxb/rb2*cosB*y(1))))/M_PI/(1-nu));

    result(2,2) = jump(0)*(0.25*((2-2*nu)*(N1*rFib_ry3*cotB-y(1)/(W6*W6)*W5*(yzb/rb+1)
                                            - 0.5*y(1)/W6*a/rb3*2*yzb+y(1)*cosB/(W7*W7)*W2*W3+0.5*y(1)*cosB/W7
                                            * a/rb3*2*yzb)+y(1)/rb*(2*nu/W6+a/rb2)-0.5*y(1)*W8/rb3
                                  * (2*nu/W6+a/rb2)*2*yzb+y(1)*W8/rb
                                  * (-2*nu/(W6*W6)*(yzb/rb+1) - a/(rb2*rb2)*2*yzb)
                                  + y(1)*cosB/rb/W7*(1-2*nu-W1/W7*W2-a*yzb/rb2)
                                  - 0.5*y(1)*W8*cosB/rb3/W7*(1-2*nu-W1/W7*W2-a*yzb/rb2)*2
                                  * yzb-y(1)*W8*cosB/rb/(W7*W7)*(1-2*nu-W1/W7*W2-a*yzb/rb2)*W3
                                  + y(1)*W8*cosB/rb/W7*(-(cosB*yzb/rb+1)/W7*W2+W1/(W7*W7)*W2*W3+0.5*
                                                        W1/W7*a/rb3*2*yzb-a/rb2+a*yzb/(rb2*rb2)*2*yzb))/M_PI/(1-nu))+
      jump(1)*(0.25*((-2+2*nu)*N1*cotB*((yzb/rb+1)/W6-cosB*W3/W7)+(2-2*nu)*
                      y(0)/(W6*W6)*W5*(yzb/rb+1)+0.5*(2-2*nu)*y(0)/W6*a/rb3*2*yzb
                      + (2-2*nu)*sinB/W7*W2-(2-2*nu)*zxb/(W7*W7)*W2*W3
                      - 0.5*(2-2*nu)*zxb/W7*a/rb3*2*yzb+1/rb*(N1*cotB-2*nu*y(0)/W6-a*y(0)/rb2)
                      - 0.5*W8/rb3*(N1*cotB-2*nu*y(0)/W6-a*y(0)/rb2)*2*yzb
                      + W8/rb*(2*nu*y(0)/(W6*W6)*(yzb/rb+1)+a*y(0)/(rb2*rb2)*2*yzb)
                      - 1/W7*(cosB*sinB+W1*cotB/rb*((2-2*nu)*cosB-W1/W7)
                              + a/rb*(sinB-yzb*zxb/rb2-zxb*W1/rb/W7))
                      + W8/(W7*W7)*(cosB*sinB+W1*cotB/rb*((2-2*nu)*cosB-W1/W7)
                                    + a/rb*(sinB-yzb*zxb/rb2-zxb*W1/rb/W7))*W3
                      - W8/W7*((cosB*yzb/rb+1)*cotB/rb*((2-2*nu)*cosB-W1/W7)
                               - 0.5*W1*cotB/rb3*((2-2*nu)*cosB-W1/W7)*2*yzb
                               + W1*cotB/rb*(-(cosB*yzb/rb+1)/W7 + W1/(W7*W7)*W3)
                               - 0.5*a/rb3*(sinB-yzb*zxb/rb2-zxb*W1/rb/W7)*2*yzb
                               + a/rb*(-zxb/rb2-yzb*sinB/rb2+yzb*zxb/(rb2*rb2)*2*yzb
                                       - sinB*W1/rb/W7-zxb*(cosB*yzb/rb+1)/rb/W7
                                       + 0.5*zxb*W1/rb3/W7*2*yzb
                                       + zxb*W1/rb/(W7*W7)*W3)))/M_PI/(1-nu))
      + jump(2)*(0.25*((2-2*nu)*rFib_ry3-(2-2*nu)*y(1)*sinB/(W7*W7)*W2*W3-0.5*
                        (2-2*nu)*y(1)*sinB/W7*a/rb3*2*yzb
                        + y(1)*sinB/rb/W7*(1+W1/W7*W2+a*yzb/rb2)
                        - 0.5*y(1)*W8*sinB/rb3/W7*(1+W1/W7*W2+a*yzb/rb2)*2*yzb
                        - y(1)*W8*sinB/rb/(W7*W7)*(1+W1/W7*W2+a*yzb/rb2)*W3
                        + y(1)*W8*sinB/rb/W7*((cosB*yzb/rb+1)/W7*W2-W1/(W7*W7)*W2*W3
                                              - 0.5*W1/W7*a/rb3*2*yzb
                                              + a/rb2-a*yzb/(rb2*rb2)*2*yzb))/M_PI/(1-nu));

    result(0,1) = jump(0)/2*(0.25*((-2+2*nu)*N1*rFib_ry2*cotB*cotB+N1/W6*((1-W5)*cotB-y(0)/
    W6*W4)-N1*(y(1)*y(1))/(W6*W6)*((1-W5)*cotB-y(0)/W6*W4)/rb+N1*y(1)/W6*
    (a/rb3*y(1)*cotB+y(0)/(W6*W6)*W4/rb*y(1)+y(1)/W6*a/rb3*y(0))+N1*
    cosB*cotB/W7*W2-N1*(y(1)*y(1))*cosB*cotB/(W7*W7)*W2/rb-N1*(y(1)*y(1))*cosB*
    cotB/W7*a/rb3+a*W8*cotB/rb3-3*a*(y(1)*y(1))*W8*cotB/(rb2*rb3)+W8/
    rb/W6*(-N1*cotB+y(0)/W6*W5+a*y(0)/rb2)-(y(1)*y(1))*W8/rb3/W6*(-N1*
    cotB+y(0)/W6*W5+a*y(0)/rb2)-(y(1)*y(1))*W8/rb2/(W6*W6)*(-N1*cotB+y(0)/
    W6*W5+a*y(0)/rb2)+y(1)*W8/rb/W6*(-y(0)/(W6*W6)*W5/rb*y(1)-y(1)/W6*
    a/rb3*y(0)-2*a*y(0)/(rb2*rb2)*y(1))+W8/rb/W7*(cosB/W7*(W1*(N1*
    cosB-a/rb)*cotB+(2-2*nu)*(rb*sinB-y(0))*cosB)-a*yzb*cosB*cotB/
    rb2)-(y(1)*y(1))*W8/rb3/W7*(cosB/W7*(W1*(N1*cosB-a/rb)*cotB+(2-
    2*nu)*(rb*sinB-y(0))*cosB)-a*yzb*cosB*cotB/rb2)-(y(1)*y(1))*W8/rb2/
    (W7*W7)*(cosB/W7*(W1*(N1*cosB-a/rb)*cotB+(2-2*nu)*(rb*sinB-y(0))*
    cosB)-a*yzb*cosB*cotB/rb2)+y(1)*W8/rb/W7*(-cosB/(W7*W7)*(W1*
    (N1*cosB-a/rb)*cotB+(2-2*nu)*(rb*sinB-y(0))*cosB)/rb*y(1)+cosB/
    W7*(1/rb*cosB*y(1)*(N1*cosB-a/rb)*cotB+W1*a/rb3*y(1)*cotB+(2-2*
    nu)/rb*sinB*y(1)*cosB)+2*a*yzb*cosB*cotB/(rb2*rb2)*y(1)))/M_PI/(1-nu))+
      jump(1)/2*(0.25*(N1*(((2-2*nu)*cotB*cotB+nu)/rb*y(1)/W6-((2-2*nu)*cotB*cotB+1)*
    cosB/rb*y(1)/W7)-N1/(W6*W6)*(-N1*y(0)*cotB+nu*yzb-a+a*y(0)*cotB/rb+
    (y(0)*y(0))/W6*W4)/rb*y(1)+N1/W6*(-a*y(0)*cotB/rb3*y(1)-(y(0)*y(0))/(W6*W6)*W4/rb*y(1)-(y(0)*y(0))/W6*a/rb3*y(1))+N1*cotB/(W7*W7)*(zxb*cosB-a*
    (rb*sinB-y(0))/rb/cosB)/rb*y(1)-N1*cotB/W7*(-a/rb2*sinB*y(1)/
    cosB+a*(rb*sinB-y(0))/rb3/cosB*y(1))+3*a*y(1)*W8*cotB/(rb2*rb3)*y(0)-
    W8/(W6*W6)*(2*nu+1/rb*(N1*y(0)*cotB+a)-(y(0)*y(0))/rb/W6*W5-a*(y(0)*y(0))/
    rb3)/rb*y(1)+W8/W6*(-1/rb3*(N1*y(0)*cotB+a)*y(1)+(y(0)*y(0))/rb3/W6*W5*y(1)+(y(0)*y(0))/rb2/(W6*W6)*W5*y(1)+(y(0)*y(0))/(rb2*rb2)/W6*a*y(1)+3*
    a*(y(0)*y(0))/(rb2*rb3)*y(1))-W8*cotB/(W7*W7)*(-cosB*sinB+a*y(0)*yzb/rb3/
    cosB+(rb*sinB-y(0))/rb*((2-2*nu)*cosB-W1/W7*W9))/rb*y(1)+W8*cotB/
    W7*(-3*a*y(0)*yzb/(rb2*rb3)/cosB*y(1)+1/rb2*sinB*y(1)*((2-2*nu)*cosB-
    W1/W7*W9)-(rb*sinB-y(0))/rb3*((2-2*nu)*cosB-W1/W7*W9)*y(1)+(rb*
    sinB-y(0))/rb*(-1/rb*cosB*y(1)/W7*W9+W1/(W7*W7)*W9/rb*y(1)+W1/W7*
    a/rb3/cosB*y(1))))/M_PI/(1-nu))+
    jump(2)/2*(0.25*(N1*(1/W6*(1+a/rb)-(y(1)*y(1))/(W6*W6)*(1+a/rb)/rb-(y(1)*y(1))/
    W6*a/rb3-cosB/W7*W2+(y(1)*y(1))*cosB/(W7*W7)*W2/rb+(y(1)*y(1))*cosB/W7*
    a/rb3)-W8/rb*(a/rb2+1/W6)+(y(1)*y(1))*W8/rb3*(a/rb2+1/W6)-
    y(1)*W8/rb*(-2*a/(rb2*rb2)*y(1)-1/(W6*W6)/rb*y(1))+W8*cosB/rb/W7*
    (W1/W7*W2+a*yzb/rb2)-(y(1)*y(1))*W8*cosB/rb3/W7*(W1/W7*W2+a*
    yzb/rb2)-(y(1)*y(1))*W8*cosB/rb2/(W7*W7)*(W1/W7*W2+a*yzb/rb2)+y(1)*
    W8*cosB/rb/W7*(1/rb*cosB*y(1)/W7*W2-W1/(W7*W7)*W2/rb*y(1)-W1/
    W7*a/rb3*y(1)-2*a*yzb/(rb2*rb2)*y(1)))/M_PI/(1-nu))+
    jump(0)/2*(0.25*(N1*(((2-2*nu)*cotB*cotB-nu)/rb*y(0)/W6-((2-2*nu)*cotB*cotB+1-
    2*nu)*cosB*(y(0)/rb-sinB)/W7)+N1/(W6*W6)*(y(0)*cotB*(1-W5)+nu*yzb-
    a+(y(1)*y(1))/W6*W4)/rb*y(0)-N1/W6*((1-W5)*cotB+a*(y(0)*y(0))*cotB/rb3-
    (y(1)*y(1))/(W6*W6)*W4/rb*y(0)-(y(1)*y(1))/W6*a/rb3*y(0))-N1*cosB*cotB/W7*
    W2+N1*zxb*cotB/(W7*W7)*W2*(y(0)/rb-sinB)+N1*zxb*cotB/W7*a/rb3*y(0)-a*W8*cotB/rb3+3*a*(y(0)*y(0))*W8*cotB/(rb2*rb3)-W8/(W6*W6)*(-2*
    nu+1/rb*(N1*y(0)*cotB-a)+(y(1)*y(1))/rb/W6*W5+a*(y(1)*y(1))/rb3)/rb*
    y(0)+W8/W6*(-1/rb3*(N1*y(0)*cotB-a)*y(0)+1/rb*N1*cotB-(y(1)*y(1))/
    rb3/W6*W5*y(0)-(y(1)*y(1))/rb2/(W6*W6)*W5*y(0)-(y(1)*y(1))/(rb2*rb2)/W6*a*y(0)-
    3*a*(y(1)*y(1))/(rb2*rb3)*y(0))-W8/(W7*W7)*((cosB*cosB)-1/rb*(N1*zxb*cotB+a*
    cosB)+a*yzb*zxb*cotB/rb3-1/rb/W7*((y(1)*y(1))*(cosB*cosB)-a*zxb*cotB/
    rb*W1))*(y(0)/rb-sinB)+W8/W7*(1/rb3*(N1*zxb*cotB+a*cosB)*
    y(0)-1/rb*N1*cosB*cotB+a*yzb*cosB*cotB/rb3-3*a*yzb*zxb*cotB/
    (rb2*rb3)*y(0)+1/rb3/W7*((y(1)*y(1))*(cosB*cosB)-a*zxb*cotB/rb*W1)*y(0)+1/
    rb/(W7*W7)*((y(1)*y(1))*(cosB*cosB)-a*zxb*cotB/rb*W1)*(y(0)/rb-sinB)-1/rb/
    W7*(-a*cosB*cotB/rb*W1+a*zxb*cotB/rb3*W1*y(0)-a*zxb*cotB/
    rb2*cosB*y(0))))/M_PI/(1-nu))+
      jump(1)/2*(0.25*((2-2*nu)*N1*rFib_ry1*cotB*cotB-N1*y(1)/(W6*W6)*((W5-1)*cotB+
    y(0)/W6*W4)/rb*y(0)+N1*y(1)/W6*(-a/rb3*y(0)*cotB+1/W6*W4-(y(0)*y(0))/(W6*W6)*W4/rb-(y(0)*y(0))/W6*a/rb3)+N1*y(1)*cotB/(W7*W7)*W9*(y(0)/
                                                                                                                    rb-sinB)+N1*y(1)*cotB/W7*a/rb3/cosB*y(0)+3*a*y(1)*W8*cotB/(rb3*rb2)*y(0)-y(1)*W8/rb3/W6*(N1*cotB-2*nu*y(0)/W6-a*y(0)/rb*(1/rb+1/
    W6))*y(0)-y(1)*W8/rb2/(W6*W6)*(N1*cotB-2*nu*y(0)/W6-a*y(0)/rb*(1/
    rb+1/W6))*y(0)+y(1)*W8/rb/W6*(-2*nu/W6+2*nu*(y(0)*y(0))/(W6*W6)/rb-a/
    rb*(1/rb+1/W6)+a*(y(0)*y(0))/rb3*(1/rb+1/W6)-a*y(0)/rb*(-1/
    rb3*y(0)-1/(W6*W6)/rb*y(0)))-y(1)*W8*cotB/rb3/W7*((-2+2*nu)*
    cosB+W1/W7*W9+a*yzb/rb2/cosB)*y(0)-y(1)*W8*cotB/rb/(W7*W7)*((-2+
    2*nu)*cosB+W1/W7*W9+a*yzb/rb2/cosB)*(y(0)/rb-sinB)+y(1)*W8*
    cotB/rb/W7*(1/rb*cosB*y(0)/W7*W9-W1/(W7*W7)*W9*(y(0)/rb-sinB)-
    W1/W7*a/rb3/cosB*y(0)-2*a*yzb/(rb2*rb2)/cosB*y(0)))/M_PI/(1-nu))+
    jump(2)/2*(0.25*(N1*(-sinB*(y(0)/rb-sinB)/W7-1/W6*(1+a/rb)+(y(0)*y(0))/(W6*W6)*(1+a/rb)/rb+(y(0)*y(0))/W6*a/rb3+cosB/W7*W2-zxb/(W7*W7)*W2*
    (y(0)/rb-sinB)-zxb/W7*a/rb3*y(0))+W8/rb*(a/rb2+1/W6)-(y(0)*y(0))*
    W8/rb3*(a/rb2+1/W6)+y(0)*W8/rb*(-2*a/(rb2*rb2)*y(0)-1/(W6*W6)/
    rb*y(0))+W8/(W7*W7)*(sinB*(cosB-a/rb)+zxb/rb*(1+a*yzb/rb2)-1/
    rb/W7*((y(1)*y(1))*cosB*sinB-a*zxb/rb*W1))*(y(0)/rb-sinB)-W8/W7*
    (sinB*a/rb3*y(0)+cosB/rb*(1+a*yzb/rb2)-zxb/rb3*(1+a*yzb/
    rb2)*y(0)-2*zxb/(rb2*rb3)*a*yzb*y(0)+1/rb3/W7*((y(1)*y(1))*cosB*sinB-a*
    zxb/rb*W1)*y(0)+1/rb/(W7*W7)*((y(1)*y(1))*cosB*sinB-a*zxb/rb*W1)*
    (y(0)/rb-sinB)-1/rb/W7*(-a*cosB/rb*W1+a*zxb/rb3*W1*y(0)-a*
    zxb/rb2*cosB*y(0))))/M_PI/(1-nu));

    result(0,2) = jump(0)/2*(0.25*((-2+2*nu)*N1*rFib_ry3*cotB*cotB-N1*y(1)/(W6*W6)*((1-W5)*
    cotB-y(0)/W6*W4)*(yzb/rb+1)+N1*y(1)/W6*(0.5*a/rb3*2*yzb*cotB+
    y(0)/(W6*W6)*W4*(yzb/rb+1)+0.5*y(0)/W6*a/rb3*2*yzb)-N1*y(1)*cosB*
    cotB/(W7*W7)*W2*W3-0.5*N1*y(1)*cosB*cotB/W7*a/rb3*2*yzb+a/
    rb3*y(1)*cotB-1.5*a*y(1)*W8*cotB/(rb2*rb3)*2*yzb+y(1)/rb/W6*(-N1*
    cotB+y(0)/W6*W5+a*y(0)/rb2)-0.5*y(1)*W8/rb3/W6*(-N1*cotB+y(0)/
    W6*W5+a*y(0)/rb2)*2*yzb-y(1)*W8/rb/(W6*W6)*(-N1*cotB+y(0)/W6*W5+
    a*y(0)/rb2)*(yzb/rb+1)+y(1)*W8/rb/W6*(-y(0)/(W6*W6)*W5*(yzb/rb+
    1)-0.5*y(0)/W6*a/rb3*2*yzb-a*y(0)/(rb2*rb2)*2*yzb)+y(1)/rb/W7*
    (cosB/W7*(W1*(N1*cosB-a/rb)*cotB+(2-2*nu)*(rb*sinB-y(0))*cosB)-
    a*yzb*cosB*cotB/rb2)-0.5*y(1)*W8/rb3/W7*(cosB/W7*(W1*(N1*
    cosB-a/rb)*cotB+(2-2*nu)*(rb*sinB-y(0))*cosB)-a*yzb*cosB*cotB/
    rb2)*2*yzb-y(1)*W8/rb/(W7*W7)*(cosB/W7*(W1*(N1*cosB-a/rb)*cotB+
    (2-2*nu)*(rb*sinB-y(0))*cosB)-a*yzb*cosB*cotB/rb2)*W3+y(1)*W8/rb/
    W7*(-cosB/(W7*W7)*(W1*(N1*cosB-a/rb)*cotB+(2-2*nu)*(rb*sinB-y(0))*
    cosB)*W3+cosB/W7*((cosB*yzb/rb+1)*(N1*cosB-a/rb)*cotB+0.5*W1*
    a/rb3*2*yzb*cotB+0.5*(2-2*nu)/rb*sinB*2*yzb*cosB)-a*cosB*
    cotB/rb2+a*yzb*cosB*cotB/(rb2*rb2)*2*yzb))/M_PI/(1-nu))+
  jump(1)/2*(0.25*(N1*(((2-2*nu)*cotB*cotB+nu)*(yzb/rb+1)/W6-((2-2*nu)*(cotB*cotB)+1)*cosB*W3/W7)-N1/(W6*W6)*(-N1*y(0)*cotB+nu*yzb-a+a*y(0)*cotB/
    rb+(y(0)*y(0))/W6*W4)*(yzb/rb+1)+N1/W6*(nu-0.5*a*y(0)*cotB/rb3*2*
    yzb-(y(0)*y(0))/(W6*W6)*W4*(yzb/rb+1)-0.5*(y(0)*y(0))/W6*a/rb3*2*yzb)+
    N1*cotB/(W7*W7)*(zxb*cosB-a*(rb*sinB-y(0))/rb/cosB)*W3-N1*cotB/
    W7*(cosB*sinB-0.5*a/rb2*sinB*2*yzb/cosB+0.5*a*(rb*sinB-y(0))/
    rb3/cosB*2*yzb)-a/rb3*y(0)*cotB+1.5*a*y(0)*W8*cotB/(rb2*rb3)*2*
    yzb+1/W6*(2*nu+1/rb*(N1*y(0)*cotB+a)-(y(0)*y(0))/rb/W6*W5-a*(y(0)*y(0))/
    rb3)-W8/(W6*W6)*(2*nu+1/rb*(N1*y(0)*cotB+a)-(y(0)*y(0))/rb/W6*W5-a*
    (y(0)*y(0))/rb3)*(yzb/rb+1)+W8/W6*(-0.5/rb3*(N1*y(0)*cotB+a)*2*
    yzb+0.5*(y(0)*y(0))/rb3/W6*W5*2*yzb+(y(0)*y(0))/rb/(W6*W6)*W5*(yzb/rb+
    1)+0.5*(y(0)*y(0))/(rb2*rb2)/W6*a*2*yzb+1.5*a*(y(0)*y(0))/(rb2*rb3)*2*yzb)+
    cotB/W7*(-cosB*sinB+a*y(0)*yzb/rb3/cosB+(rb*sinB-y(0))/rb*((2-
    2*nu)*cosB-W1/W7*W9))-W8*cotB/(W7*W7)*(-cosB*sinB+a*y(0)*yzb/rb3/cosB+(rb*sinB-y(0))/rb*((2-2*nu)*cosB-W1/W7*W9))*W3+W8*cotB/
    W7*(a/rb3/cosB*y(0)-1.5*a*y(0)*yzb/(rb2*rb3)/cosB*2*yzb+0.5/
    rb2*sinB*2*yzb*((2-2*nu)*cosB-W1/W7*W9)-0.5*(rb*sinB-y(0))/rb3*((2-2*nu)*cosB-W1/W7*W9)*2*yzb+(rb*sinB-y(0))/rb*(-(cosB*yzb/
    rb+1)/W7*W9+W1/(W7*W7)*W9*W3+0.5*W1/W7*a/rb3/cosB*2*
    yzb)))/M_PI/(1-nu))+
    jump(2)/2*(0.25*(N1*(-y(1)/(W6*W6)*(1+a/rb)*(yzb/rb+1)-0.5*y(1)/W6*a/
    rb3*2*yzb+y(1)*cosB/(W7*W7)*W2*W3+0.5*y(1)*cosB/W7*a/rb3*2*
    yzb)-y(1)/rb*(a/rb2+1/W6)+0.5*y(1)*W8/rb3*(a/rb2+1/W6)*2*
    yzb-y(1)*W8/rb*(-a/(rb2*rb2)*2*yzb-1/(W6*W6)*(yzb/rb+1))+y(1)*cosB/
    rb/W7*(W1/W7*W2+a*yzb/rb2)-0.5*y(1)*W8*cosB/rb3/W7*(W1/
    W7*W2+a*yzb/rb2)*2*yzb-y(1)*W8*cosB/rb/(W7*W7)*(W1/W7*W2+a*
    yzb/rb2)*W3+y(1)*W8*cosB/rb/W7*((cosB*yzb/rb+1)/W7*W2-W1/
    (W7*W7)*W2*W3-0.5*W1/W7*a/rb3*2*yzb+a/rb2-a*yzb/(rb2*rb2)*2*
    yzb))/M_PI/(1-nu))+
    jump(0)/2*(0.25*((2-2*nu)*(N1*rFib_ry1*cotB-y(0)/(W6*W6)*W5/rb*y(1)-y(1)/W6*
    a/rb3*y(0)+y(1)*cosB/(W7*W7)*W2*(y(0)/rb-sinB)+y(1)*cosB/W7*a/rb3*y(0))-y(1)*W8/rb3*(2*nu/W6+a/rb2)*y(0)+y(1)*W8/rb*(-2*nu/(W6*W6)/rb*y(0)-2*a/(rb2*rb2)*y(0))-y(1)*W8*cosB/rb3/W7*(1-2*nu-W1/W7*
    W2-a*yzb/rb2)*y(0)-y(1)*W8*cosB/rb/(W7*W7)*(1-2*nu-W1/W7*W2-a*
    yzb/rb2)*(y(0)/rb-sinB)+y(1)*W8*cosB/rb/W7*(-1/rb*cosB*y(0)/W7*
                                            W2+W1/(W7*W7)*W2*(y(0)/rb-sinB)+W1/W7*a/rb3*y(0)+2*a*yzb/(rb2*rb2)*y(0)))/M_PI/(1-nu))+
    jump(1)/2*(0.25*((-2+2*nu)*N1*cotB*(1/rb*y(0)/W6-cosB*(y(0)/rb-sinB)/W7)-
    (2-2*nu)/W6*W5+(2-2*nu)*(y(0)*y(0))/(W6*W6)*W5/rb+(2-2*nu)*(y(0)*y(0))/W6*
    a/rb3+(2-2*nu)*cosB/W7*W2-(2-2*nu)*zxb/(W7*W7)*W2*(y(0)/rb-
    sinB)-(2-2*nu)*zxb/W7*a/rb3*y(0)-W8/rb3*(N1*cotB-2*nu*y(0)/
    W6-a*y(0)/rb2)*y(0)+W8/rb*(-2*nu/W6+2*nu*(y(0)*y(0))/(W6*W6)/rb-a/rb2+
    2*a*(y(0)*y(0))/(rb2*rb2))+W8/(W7*W7)*(cosB*sinB+W1*cotB/rb*((2-2*nu)*
    cosB-W1/W7)+a/rb*(sinB-yzb*zxb/rb2-zxb*W1/rb/W7))*(y(0)/rb-
    sinB)-W8/W7*(1/rb2*cosB*y(0)*cotB*((2-2*nu)*cosB-W1/W7)-W1*
    cotB/rb3*((2-2*nu)*cosB-W1/W7)*y(0)+W1*cotB/rb*(-1/rb*cosB*
    y(0)/W7+W1/(W7*W7)*(y(0)/rb-sinB))-a/rb3*(sinB-yzb*zxb/rb2-
    zxb*W1/rb/W7)*y(0)+a/rb*(-yzb*cosB/rb2+2*yzb*zxb/(rb2*rb2)*y(0)-
    cosB*W1/rb/W7-zxb/rb2*cosB*y(0)/W7+zxb*W1/rb3/W7*y(0)+zxb*
    W1/rb/(W7*W7)*(y(0)/rb-sinB))))/M_PI/(1-nu))+
    jump(2)/2*(0.25*((2-2*nu)*rFib_ry1-(2-2*nu)*y(1)*sinB/(W7*W7)*W2*(y(0)/rb-
    sinB)-(2-2*nu)*y(1)*sinB/W7*a/rb3*y(0)-y(1)*W8*sinB/rb3/W7*(1+
    W1/W7*W2+a*yzb/rb2)*y(0)-y(1)*W8*sinB/rb/(W7*W7)*(1+W1/W7*W2+
    a*yzb/rb2)*(y(0)/rb-sinB)+y(1)*W8*sinB/rb/W7*(1/rb*cosB*y(0)/
    W7*W2-W1/(W7*W7)*W2*(y(0)/rb-sinB)-W1/W7*a/rb3*y(0)-2*a*yzb/
    (rb2*rb2)*y(0)))/M_PI/(1-nu));

    result(1,2) = jump(0)/2*(0.25*(N1*(((2-2*nu)*cotB*cotB-nu)*(yzb/rb+1)/W6-((2-2*nu)*
    cotB*cotB+1-2*nu)*cosB*W3/W7)+N1/(W6*W6)*(y(0)*cotB*(1-W5)+nu*yzb-a+
    (y(1)*y(1))/W6*W4)*(yzb/rb+1)-N1/W6*(0.5*a*y(0)*cotB/rb3*2*yzb+
    nu-(y(1)*y(1))/(W6*W6)*W4*(yzb/rb+1)-0.5*(y(1)*y(1))/W6*a/rb3*2*yzb)-N1*
    sinB*cotB/W7*W2+N1*zxb*cotB/(W7*W7)*W2*W3+0.5*N1*zxb*cotB/W7*
    a/rb3*2*yzb-a/rb3*y(0)*cotB+1.5*a*y(0)*W8*cotB/(rb2*rb3)*2*yzb+
    1/W6*(-2*nu+1/rb*(N1*y(0)*cotB-a)+(y(1)*y(1))/rb/W6*W5+a*(y(1)*y(1))/
    rb3)-W8/(W6*W6)*(-2*nu+1/rb*(N1*y(0)*cotB-a)+(y(1)*y(1))/rb/W6*W5+
    a*(y(1)*y(1))/rb3)*(yzb/rb+1)+W8/W6*(-0.5/rb3*(N1*y(0)*cotB-a)*
    2*yzb-0.5*(y(1)*y(1))/rb3/W6*W5*2*yzb-(y(1)*y(1))/rb/(W6*W6)*W5*(yzb/
    rb+1)-0.5*(y(1)*y(1))/(rb2*rb2)/W6*a*2*yzb-1.5*a*(y(1)*y(1))/(rb2*rb3)*2*yzb)+
    1/W7*((cosB*cosB)-1/rb*(N1*zxb*cotB+a*cosB)+a*yzb*zxb*cotB/rb3-1/rb/W7*((y(1)*y(1))*(cosB*cosB)-a*zxb*cotB/rb*W1))-W8/(W7*W7)*((cosB*cosB)-
    1/rb*(N1*zxb*cotB+a*cosB)+a*yzb*zxb*cotB/rb3-1/rb/W7*
    ((y(1)*y(1))*(cosB*cosB)-a*zxb*cotB/rb*W1))*W3+W8/W7*(0.5/rb3*(N1*
    zxb*cotB+a*cosB)*2*yzb-1/rb*N1*sinB*cotB+a*zxb*cotB/rb3+a*
                                                      yzb*sinB*cotB/rb3-1.5*a*yzb*zxb*cotB/(rb2*rb3)*2*yzb+0.5/rb3/W7*((y(1)*y(1))*(cosB*cosB)-a*zxb*cotB/rb*W1)*2*yzb+1/rb/(W7*W7)*((y(1)*y(1))*(cosB*cosB)-a*zxb*cotB/rb*W1)*W3-1/rb/W7*(-a*sinB*cotB/rb*W1+
    0.5*a*zxb*cotB/rb3*W1*2*yzb-a*zxb*cotB/rb*(cosB*yzb/rb+
    1))))/M_PI/(1-nu))+
    jump(1)/2*(0.25*((2-2*nu)*N1*rFib_ry3*cotB*cotB-N1*y(1)/(W6*W6)*((W5-1)*cotB+
    y(0)/W6*W4)*(yzb/rb+1)+N1*y(1)/W6*(-0.5*a/rb3*2*yzb*cotB-y(0)/
    (W6*W6)*W4*(yzb/rb+1)-0.5*y(0)/W6*a/rb3*2*yzb)+N1*y(1)*cotB/
    (W7*W7)*W9*W3+0.5*N1*y(1)*cotB/W7*a/rb3/cosB*2*yzb-a/rb3*
    y(1)*cotB+1.5*a*y(1)*W8*cotB/(rb2*rb3)*2*yzb+y(1)/rb/W6*(N1*cotB-2*
    nu*y(0)/W6-a*y(0)/rb*(1/rb+1/W6))-0.5*y(1)*W8/rb3/W6*(N1*
    cotB-2*nu*y(0)/W6-a*y(0)/rb*(1/rb+1/W6))*2*yzb-y(1)*W8/rb/(W6*W6)*(N1*cotB-2*nu*y(0)/W6-a*y(0)/rb*(1/rb+1/W6))*(yzb/rb+1)+y(1)*
    W8/rb/W6*(2*nu*y(0)/(W6*W6)*(yzb/rb+1)+0.5*a*y(0)/rb3*(1/rb+
    1/W6)*2*yzb-a*y(0)/rb*(-0.5/rb3*2*yzb-1/(W6*W6)*(yzb/rb+
    1)))+y(1)*cotB/rb/W7*((-2+2*nu)*cosB+W1/W7*W9+a*yzb/rb2/cosB)-
    0.5*y(1)*W8*cotB/rb3/W7*((-2+2*nu)*cosB+W1/W7*W9+a*yzb/
    rb2/cosB)*2*yzb-y(1)*W8*cotB/rb/(W7*W7)*((-2+2*nu)*cosB+W1/W7*
    W9+a*yzb/rb2/cosB)*W3+y(1)*W8*cotB/rb/W7*((cosB*yzb/rb+1)/
    W7*W9-W1/(W7*W7)*W9*W3-0.5*W1/W7*a/rb3/cosB*2*yzb+a/rb2/
    cosB-a*yzb/(rb2*rb2)/cosB*2*yzb))/M_PI/(1-nu))+
    jump(2)/2*(0.25*(N1*(-sinB*W3/W7+y(0)/(W6*W6)*(1+a/rb)*(yzb/rb+1)+
    0.5*y(0)/W6*a/rb3*2*yzb+sinB/W7*W2-zxb/(W7*W7)*W2*W3-0.5*
    zxb/W7*a/rb3*2*yzb)+y(0)/rb*(a/rb2+1/W6)-0.5*y(0)*W8/rb3*(a/rb2+1/W6)*2*yzb+y(0)*W8/rb*(-a/(rb2*rb2)*2*yzb-1/(W6*W6)*
    (yzb/rb+1))-1/W7*(sinB*(cosB-a/rb)+zxb/rb*(1+a*yzb/rb2)-1/
    rb/W7*((y(1)*y(1))*cosB*sinB-a*zxb/rb*W1))+W8/(W7*W7)*(sinB*(cosB-
    a/rb)+zxb/rb*(1+a*yzb/rb2)-1/rb/W7*((y(1)*y(1))*cosB*sinB-a*zxb/
    rb*W1))*W3-W8/W7*(0.5*sinB*a/rb3*2*yzb+sinB/rb*(1+a*yzb/
    rb2)-0.5*zxb/rb3*(1+a*yzb/rb2)*2*yzb+zxb/rb*(a/rb2-a*
    yzb/(rb2*rb2)*2*yzb)+0.5/rb3/W7*((y(1)*y(1))*cosB*sinB-a*zxb/rb*
    W1)*2*yzb+1/rb/(W7*W7)*((y(1)*y(1))*cosB*sinB-a*zxb/rb*W1)*W3-1/
    rb/W7*(-a*sinB/rb*W1+0.5*a*zxb/rb3*W1*2*yzb-a*zxb/rb*
    (cosB*yzb/rb+1))))/M_PI/(1-nu))+
    jump(0)/2*(0.25*((2-2*nu)*(N1*rFib_ry2*cotB+1/W6*W5-(y(1)*y(1))/(W6*W6)*W5/
    rb-(y(1)*y(1))/W6*a/rb3-cosB/W7*W2+(y(1)*y(1))*cosB/(W7*W7)*W2/rb+(y(1)*y(1))*
    cosB/W7*a/rb3)+W8/rb*(2*nu/W6+a/rb2)-(y(1)*y(1))*W8/rb3*(2*
    nu/W6+a/rb2)+y(1)*W8/rb*(-2*nu/(W6*W6)/rb*y(1)-2*a/(rb2*rb2)*y(1))+
    W8*cosB/rb/W7*(1-2*nu-W1/W7*W2-a*yzb/rb2)-(y(1)*y(1))*W8*cosB/
               rb3/W7*(1-2*nu-W1/W7*W2-a*yzb/rb2)-(y(1)*y(1))*W8*cosB/rb2/(W7*W7)*(1-2*nu-W1/W7*W2-a*yzb/rb2)+y(1)*W8*cosB/rb/W7*(-1/rb*
    cosB*y(1)/W7*W2+W1/(W7*W7)*W2/rb*y(1)+W1/W7*a/rb3*y(1)+2*a*
    yzb/(rb2*rb2)*y(1)))/M_PI/(1-nu))+
    jump(1)/2*(0.25*((-2+2*nu)*N1*cotB*(1/rb*y(1)/W6-cosB/rb*y(1)/W7)+(2-
    2*nu)*y(0)/(W6*W6)*W5/rb*y(1)+(2-2*nu)*y(0)/W6*a/rb3*y(1)-(2-2*
    nu)*zxb/(W7*W7)*W2/rb*y(1)-(2-2*nu)*zxb/W7*a/rb3*y(1)-W8/rb3*(N1*cotB-2*nu*y(0)/W6-a*y(0)/rb2)*y(1)+W8/rb*(2*nu*y(0)/(W6*W6)/
    rb*y(1)+2*a*y(0)/(rb2*rb2)*y(1))+W8/(W7*W7)*(cosB*sinB+W1*cotB/rb*((2-
    2*nu)*cosB-W1/W7)+a/rb*(sinB-yzb*zxb/rb2-zxb*W1/rb/W7))/
    rb*y(1)-W8/W7*(1/rb2*cosB*y(1)*cotB*((2-2*nu)*cosB-W1/W7)-W1*
    cotB/rb3*((2-2*nu)*cosB-W1/W7)*y(1)+W1*cotB/rb*(-cosB/rb*
    y(1)/W7+W1/(W7*W7)/rb*y(1))-a/rb3*(sinB-yzb*zxb/rb2-zxb*W1/
    rb/W7)*y(1)+a/rb*(2*yzb*zxb/(rb2*rb2)*y(1)-zxb/rb2*cosB*y(1)/W7+
    zxb*W1/rb3/W7*y(1)+zxb*W1/rb2/(W7*W7)*y(1))))/M_PI/(1-nu))+
    jump(2)/2*(0.25*((2-2*nu)*rFib_ry2+(2-2*nu)*sinB/W7*W2-(2-2*nu)*(y(1)*y(1))*
    sinB/(W7*W7)*W2/rb-(2-2*nu)*(y(1)*y(1))*sinB/W7*a/rb3+W8*sinB/rb/
    W7*(1+W1/W7*W2+a*yzb/rb2)-(y(1)*y(1))*W8*sinB/rb3/W7*(1+W1/
    W7*W2+a*yzb/rb2)-(y(1)*y(1))*W8*sinB/rb2/(W7*W7)*(1+W1/W7*W2+a*
    yzb/rb2)+y(1)*W8*sinB/rb/W7*(1/rb*cosB*y(1)/W7*W2-W1/(W7*W7)*
    W2/rb*y(1)-W1/W7*a/rb3*y(1)-2*a*yzb/(rb2*rb2)*y(1)))/M_PI/(1-nu));

    return result;
  }
}
