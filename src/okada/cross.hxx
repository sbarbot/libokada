#pragma once

#include <FTensor.hpp>

namespace okada
{
  inline
  FTensor::Tensor1<double,3>
  cross(const FTensor::Tensor1<double,3> &v,
        const FTensor::Tensor1<double,3> &w)
  {
    FTensor::Tensor1<double,3> result;
    result(0)=v(1)*w(2) - w(1)*v(2);
    result(1)=v(2)*w(0) - w(2)*v(0);
    result(2)=v(0)*w(1) - w(0)*v(1);
    return result;
  }
}
