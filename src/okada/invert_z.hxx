#pragma once

#include <FTensor.hpp>

namespace okada
{
  inline FTensor::Tensor1<double,3> invert_z(const FTensor::Tensor1<double,3> &in)
  {
    return FTensor::Tensor1<double,3>(in(0),in(1),-in(2));
  }
  inline FTensor::Tensor2_symmetric<double,3>
  invert_z(const FTensor::Tensor2_symmetric<double,3> &in)
  {
    return FTensor::Tensor2_symmetric<double,3>(in(0,0),in(0,1),-in(0,2),
                                                in(1,1),-in(1,2),in(2,2));
  }
}

