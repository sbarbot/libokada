#pragma once

/// For M_PI
#define _USE_MATH_DEFINES
#include <cmath>

namespace okada
{
  inline double radians(const double &degrees)
  {
    return degrees*M_PI/180;
  }
}
