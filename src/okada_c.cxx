#include "okada.hxx"

#include <cstdlib>

extern "C"
{
  void okada_displacement (double lambda, double mu, double slip,
                           double opening,
                           double width, double length, double strike,
                           double dip, double rake, double origin[],
                           double coord[],
                           double *displacement,
                           double *strain)
  {
    try
      {
        FTensor::Tensor1<double,3> origin_tensor (origin[0], origin[1], origin[2]),
          coord_tensor (coord[0], coord[1], coord[2]);
        okada::Okada o (lambda, mu, slip, opening, width, length, strike, dip, rake,
                        origin_tensor);
        okada::Displacement result (o.displacement (coord_tensor));
        if (displacement!=NULL)
          {
            displacement[0]=result.first(0);
            displacement[1]=result.first(1);
            displacement[2]=result.first(2);
          }
        if (strain!=NULL)
          {
            strain[0]=result.second(0,0);
            strain[1]=result.second(0,1);
            strain[2]=result.second(0,2);
            strain[3]=result.second(1,1);
            strain[4]=result.second(1,2);
            strain[5]=result.second(2,2);
          }
      }
    catch (std::exception &e)
      {
        std::cerr << "libokada ERROR: " << e.what () << "\n" << std::flush;
        std::exit (0);
      }
    catch (...)
      {
        std::cerr << "libokada INTERNAL ERROR\n" << std::flush;
        std::exit (0);
      }
  }
  void OKADA_DISPLACEMENT_FORTRAN (double *lambda, double *mu, double *slip,
                                   double *opening, double *width,
                                   double *length, double *strike,
                                   double *dip, double *rake, double origin[],
                                   double coord[],
                                   double *displacement,
                                   double *strain)
  {
    okada_displacement (*lambda, *mu, *slip, *opening, *width, *length,
                        *strike, *dip, *rake, origin, coord, displacement,
                        strain);
  }
    
  void triangle_displacement (double origin[], double u[], double v[],
                              double jump[], double lambda, double mu,
                              double coord[],
                              double *displacement,
                              double *strain)
  {
    try
      {
        FTensor::Tensor1<double,3> origin_tensor(origin[0],origin[1],origin[2]),
          u_tensor(u[0],u[1],u[2]), v_tensor(v[0],v[1],v[2]),
          jump_tensor(jump[0],jump[1],jump[2]),
          coord_tensor(coord[0],coord[1],coord[2]);
        okada::Triangle_Fault triangle (origin_tensor, u_tensor, v_tensor,
                                        jump_tensor, lambda, mu);
        okada::Displacement result (triangle.displacement (coord_tensor));
        if (displacement!=NULL)
          {
            displacement[0]=result.first(0);
            displacement[1]=result.first(1);
            displacement[2]=result.first(2);
          }
        if (strain!=NULL)
          {
            strain[0]=result.second(0,0);
            strain[1]=result.second(0,1);
            strain[2]=result.second(0,2);
            strain[3]=result.second(1,1);
            strain[4]=result.second(1,2);
            strain[5]=result.second(2,2);
          }
      }
    catch (std::exception &e)
      {
        std::cerr << "libokada ERROR: " << e.what () << "\n" << std::flush;
        std::exit (0);
      }
    catch (...)
      {
        std::cerr << "libokada INTERNAL ERROR\n" << std::flush;
        std::exit (0);
      }
  }
  void TRIANGLE_DISPLACEMENT_FORTRAN (double origin[], double u[], double v[],
                                      double jump[], double *lambda, double *mu,
                                      double coord[],
                                      double *displacement,
                                      double *strain)
  {
    triangle_displacement (origin, u, v, jump, *lambda, *mu, coord,
                           displacement, strain);
  }

  void strain_volume_displacement (double origin[], double shape[],
                                   double strike,
                                   double applied_strain[],
                                   double lambda, double mu,
                                   double coord[],
                                   double *displacement,
                                   double *output_strain)
  {
    try
      {
        FTensor::Tensor1<double,3> origin_tensor(origin[0],origin[1],origin[2]),
          shape_tensor(shape[0],shape[1],shape[2]),
          coord_tensor(coord[0],coord[1],coord[2]);
        FTensor::Tensor2_symmetric<double,3> applied_strain_tensor
          (applied_strain[0],applied_strain[1],applied_strain[2],
           applied_strain[3],applied_strain[4],applied_strain[5]);

        okada::Strain_Volume strain_volume (origin_tensor, shape_tensor, strike,
                                        applied_strain_tensor, lambda, mu);
        okada::Displacement result (strain_volume.displacement (coord_tensor));
        if (displacement!=NULL)
          {
            displacement[0]=result.first(0);
            displacement[1]=result.first(1);
            displacement[2]=result.first(2);
          }
        if (output_strain!=NULL)
          {
            output_strain[0]=result.second(0,0);
            output_strain[1]=result.second(0,1);
            output_strain[2]=result.second(0,2);
            output_strain[3]=result.second(1,1);
            output_strain[4]=result.second(1,2);
            output_strain[5]=result.second(2,2);
          }
      }
    catch (std::exception &e)
      {
        std::cerr << "libokada ERROR: " << e.what () << "\n" << std::flush;
        std::exit (0);
      }
    catch (...)
      {
        std::cerr << "libokada INTERNAL ERROR\n" << std::flush;
        std::exit (0);
      }
  }
  void STRAIN_VOLUME_DISPLACEMENT_FORTRAN (double origin[], double shape[],
                                           double *strike,
                                           double applied_strain[],
                                           double *lambda,
                                           double *mu,
                                           double coord[],
                                           double *displacement,
                                           double *output_strain)
  {
    strain_volume_displacement (origin, shape, *strike, applied_strain, *lambda,
                                *mu, coord, displacement, output_strain);
  }
}
