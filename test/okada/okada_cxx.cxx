#include <cstdlib>
#include <fstream>
#include <iostream>
#include "../../src/okada.hxx"

int main(int argc, char *argv[])
{
  if(argc!=2)
    {
      std::cerr << "Need a filename to read\n";
      std::exit(0);
    }

  double lambda(1.2), mu(2.3), slip(-10), opening(3), width(.25), length(.2),
    strike(30), dip(75), rake(10);
  FTensor::Tensor1<double,3> origin(-0.1001,-0.05001,0);
  okada::Okada okada(lambda, mu, slip, opening, width, length, strike, dip,
                     rake, origin);

  okada::Displacement absolute_diff, relative_diff;
  FTensor::Index<'i',3> i;
  FTensor::Index<'j',3> j;
  absolute_diff.first(i)=0;
  absolute_diff.second(i,j)=0;
  relative_diff.first(i)=0;
  relative_diff.second(i,j)=0;
  
  std::ifstream input(argv[1]);
  FTensor::Tensor1<double,3> xyz;
  okada::Displacement d;
  input >> xyz >> d.first >> d.second;
  while(input)
    {
      okada::Displacement disp(okada.displacement(xyz));
      for(int i=0;i<3;++i)
        {
          absolute_diff.first(i)=std::max(absolute_diff.first(i),
                                          std::abs(disp.first(i)-d.first(i)));
          relative_diff.first(i)=
            std::max(relative_diff.first(i),
                     2*std::abs(disp.first(i)-d.first(i))/
                     (std::abs(disp.first(i))+std::abs(d.first(i))));
          for(int j=i;j<3;++j)
            {
              absolute_diff.second(i,j)=std::max(absolute_diff.second(i,j),
                                                 std::abs(disp.second(i,j)-d.second(i,j)));
              relative_diff.second(i,j)=
                std::max(relative_diff.second(i,j),
                         2*std::abs(disp.second(i,j)-d.second(i,j))/
                         (std::abs(disp.second(i,j))+std::abs(d.second(i,j))));
            }
        }
      input >> xyz >> d.first >> d.second;
    }
  std::cout << "Displacement: Absolute Difference: "
            << absolute_diff.first << "\n\t"
            << "Relative Difference: " << relative_diff.first << "\n"
            << "Strain: Absolute Difference: "
            << absolute_diff.second << "\n\t"
            << "Relative Difference: " << relative_diff.second << "\n";
}
