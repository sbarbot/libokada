program hello
  real*8 lambda, mu, slip, opening, width, length, strike, dip, rake
  real*8, dimension(3) :: origin, coord, displacement
  real*8, dimension(6) :: strain
  
  lambda=1.2
  mu=2.3
  slip=-10
  opening=3
  width=.25
  length=.2
  strike=30
  dip=75
  rake=10

  origin(1)=-0.1001
  origin(2)=-0.05001
  origin(3)=0
  coord(1)=0.27000000000000002
  coord(2)=0.45999999999999996
  coord(3)=0.0000000000000000
  
  call okada_displacement_f(lambda,mu,slip,opening,width,length,strike, &
       dip,rake,origin,coord,displacement,strain)

  print *, "Original Fortran Values"
  print *, "[-0.11703878883185220, -0.24255758317142168, ", &
       "-5.3610656410455704E-002]"
  print *, "[[0.35701698586819242, 0.53027513711779950, 0.0000000000000000],", &
       "[0.57538928687097957, 0.0000000000000000],[-0.19291162490844727]]"
  print *
  print *, "libokada Fortran Values"
  print *, displacement
  print *, strain
end program hello
