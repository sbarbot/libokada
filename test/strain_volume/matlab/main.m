format long g
[X,Y,Z] = meshgrid(-10.5:1:10.5,-10.5:1:10.5, 0:1:10.5);
[ue,un,uv] = computeDisplacementVerticalShearZone(X,Y,Z,
                                                  3.14,1.5,9.2,
                                                  6.5,3.5,9.2,
                                                  30,
                                                  1.1,1.2,1.3,1.4,1.5,1.6,
                                                  2.3,.25);
[Stress,Strain] = computeStressVerticalShearZone(X,Y,Z,
                                                 3.14,1.5,9.2,
                                                 6.5,3.5,9.2,
                                                 30,
                                                 1.1,1.2,1.3,1.4,1.5,1.6,
                                                 2.3,.25);

for row = 1:numel(X)
  printf("[%g, %g, %g]\n [%g, %g, %g]\n [[%g, %g, %g], [%g, %g], [%g]]\n",
         X(row),Y(row),Z(row),
         ue(row),un(row),uv(row),
         Strain(:,:,:,1)(row),
         Strain(:,:,:,2)(row),
         Strain(:,:,:,3)(row),
         Strain(:,:,:,4)(row),
         Strain(:,:,:,5)(row),
         Strain(:,:,:,6)(row));
endfor



