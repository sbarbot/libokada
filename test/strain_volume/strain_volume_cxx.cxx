#include "../../src/okada.hxx"
#include <fstream>
#include <cstdlib>
int main(int argc, char *argv[])
{
  const FTensor::Tensor1<double,3> origin(3.14,1.5,9.2);
  const FTensor::Tensor1<double,3> shape(6.5,3.5,9.2);
  const double strike(30);
  const FTensor::Tensor2_symmetric<double,3> strain(1.1,1.2,1.3,1.4,1.5,1.6);
  const double lambda(2.3), mu(2.3);
  okada::Strain_Volume strain_volume(origin,shape,strike,strain,lambda,mu);

  if(argc!=2)
    {
      std::cerr << "Need a filename to read\n";
      std::exit(0);
    }
  okada::Displacement absolute_diff, relative_diff;
  FTensor::Index<'i',3> i;
  FTensor::Index<'j',3> j;
  absolute_diff.first(i)=0;
  absolute_diff.second(i,j)=0;
  relative_diff.first(i)=0;
  relative_diff.second(i,j)=0;

  std::ifstream input(argv[1]);
  FTensor::Tensor1<double,3> xyz;
  okada::Displacement d;
  input >> xyz >> d.first >> d.second;
  while(input)
    {
      okada::Displacement disp(strain_volume.displacement(xyz));
      for(int i=0;i<3;++i)
        {
          absolute_diff.first(i)=std::max(absolute_diff.first(i),
                                          std::abs(disp.first(i)-d.first(i)));
          relative_diff.first(i)=
            std::max(relative_diff.first(i),
                     2*std::abs(disp.first(i)-d.first(i))/
                     (std::abs(disp.first(i))+std::abs(d.first(i))));

          for(int j=i;j<3;++j)
            {
              absolute_diff.second(i,j)=std::max(absolute_diff.second(i,j),
                                                 std::abs(disp.second(i,j)-d.second(i,j)));
              relative_diff.second(i,j)=
                std::max(relative_diff.second(i,j),
                         2*std::abs(disp.second(i,j)-d.second(i,j))/
                         (std::abs(disp.second(i,j))+std::abs(d.second(i,j))));
            }
        }
      input >> xyz >> d.first >> d.second;
    }
  std::cout << "Displacement: Absolute Difference: "
            << absolute_diff.first << "\n\t"
            << "Relative Difference: " << relative_diff.first << "\n"
            << "Strain: Absolute Difference: "
            << absolute_diff.second << "\n\t"
            << "Relative Difference: " << relative_diff.second << "\n"
    ;
}

