program hello
  real*8 lambda, mu, strike
  real*8, dimension(3) :: origin, shape, coord, displacement
  real*8, dimension(6) :: applied_strain, output_strain
  
  lambda=2.3
  mu=2.3
  strike=30

  origin(1)=3.14
  origin(2)=1.5
  origin(3)=9.2
  shape(1)=6.5
  shape(2)=3.5
  shape(3)=9.2
  coord(1)=9.5
  coord(2)=-8.5
  coord(3)=8
  applied_strain(1)=1.1;
  applied_strain(2)=1.2;
  applied_strain(3)=1.3;
  applied_strain(4)=1.4;
  applied_strain(5)=1.5;
  applied_strain(6)=1.6;
  
  call strain_volume_displacement_f(origin,shape,strike,applied_strain, &
       lambda,mu,coord,displacement,output_strain)

  print *, "Original Matlab Values"
  print *, "[0.0482885, -0.588411, -0.615505]"
  print *, "[[0.0156761, 0.0360055, 0.0224823], [-0.055278, ", &
       "-0.0334835], [0.0331892]]"
  print *
  print *, "libokada Fortran Values"
  print *, displacement
  print *, output_strain
end program hello
