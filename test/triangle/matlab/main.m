format long g
[X,Y,Z] = meshgrid(-10.5:1:10.5,-10.5:1:10.5,-10.5:1:0);
[ue,un,uv] = computeDisplacementNikkhoo15(X,Y,Z,[3.14 1.5 -9.2],[9.64 5.0 -18.4],[-3.96 9.7 -17.3],-9.32103,2.51949,0.103301,.25);
[Stress,Strain] = computeStressNikkhoo15(X,Y,Z,[3.14 1.5 -9.2],[9.64 5.0 -18.4],[-3.96 9.7 -17.3],-9.32103,2.51949,0.103301,1.0,1.0);

## The order of components of Strain is xx,yy,zz,xy,xz,yz

for row = 1:numel(X)
  printf("[%g, %g, %g]\n [%g, %g, %g]\n [[%g, %g, %g], [%g, %g], [%g]]\n",
         X(row),Y(row),Z(row),
         ue(row),un(row),uv(row),
         Strain(:,1)(row),
         Strain(:,4)(row),
         Strain(:,5)(row),
         Strain(:,2)(row),
         Strain(:,6)(row),
         Strain(:,3)(row));
endfor


