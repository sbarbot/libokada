program hello
  real*8 lambda, mu
  real*8, dimension(3) :: origin, u, v, jump, coord, displacement
  real*8, dimension(6) :: strain
  
  lambda=1.0
  mu=1.0

  origin(1)=3.14
  origin(2)=1.5
  origin(3)=9.2
  u(1)=6.5
  u(2)=3.5
  u(3)=9.2
  v(1)=-7.1
  v(2)=8.2
  v(3)=8.1
  jump(1)=8.2
  jump(2)=-4.6
  jump(3)=-2.2
  coord(1)=9.5
  coord(2)=-8.5
  coord(3)=8.5
  
  call triangle_displacement_f(origin,u,v,jump,lambda,mu,coord, &
       displacement,strain)

  print *, "Original Matlab Values"
  print *, "[-0.0380368, 0.178467, -0.00221999]"
  print *, "[[0.00122331, -0.00700767, 0.0082592],[0.00805871, ", &
       "-0.00893597], [-0.00121621]]"
  print *
  print *, "libokada Fortran Values"
  print *, displacement
  print *, strain
end program hello
